<?php

namespace EasyOceanEngine\SearchLaunch\PrivativeWord;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

/**
 * 否定词管理
 * Class ServiceProvider.
 */
class ServiceProvider implements ServiceProviderInterface
{
    /**
     * {@inheritdoc}.
     */
    public function register(Container $app)
    {
        $app['privative_word'] = function ($app) {
            return new Client($app);
        };
    }
}
