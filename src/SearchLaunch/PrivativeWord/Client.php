<?php

namespace EasyOceanEngine\SearchLaunch\PrivativeWord;

use EasyOceanEngine\Kernel\BaseClient;
use EasyOceanEngine\Kernel\Exceptions\Exception;
use GuzzleHttp\Exception\GuzzleException;

/**
 * 否定词管理（新版）
 */
class Client extends BaseClient
{
    /**
     * 批量新增计划否定词
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function adAdd($param, $token = null): array
    {
        return $this->setUri('tools/privative_word/ad/add/')->setPost()->setToken($token)->request($param);
    }

    /**
     * 设置计划否定词
     * 单个设置计划否定词，即全量更新单个计划否定词
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function adUpdate($param, $token = null): array
    {
        return $this->setUri('tools/privative_word/ad/update')->setPost()->setToken($token)->request($param);
    }

    /**
     * 批量新增组否定词
     * 批量新增组否定词，即增量更新组否定词
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function campaignAdd($param, $token = null): array
    {
        return $this->setUri('tools/privative_word/campaign/add')->setPost()->setToken($token)->request($param);
    }

    /**
     * 设置组否定词
     * 单个设置组否定词，即全量更新单个组否定词
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function campaignUpdate($param, $token = null): array
    {
        return $this->setUri('tools/privative_word/campaign/update/')->setPost()->setToken($token)->request($param);
    }

    /**
     * 获取否定词列表
     * 获取否定词列表，根据广告组或广告计划获取否定词
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function get($param, $token = null): array
    {
        return $this->setUri('tools/privative_word/get')->setContentType()->setToken($token)->request($param);
    }
}
