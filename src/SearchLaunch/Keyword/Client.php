<?php

namespace EasyOceanEngine\SearchLaunch\Keyword;

use EasyOceanEngine\Kernel\BaseClient;
use EasyOceanEngine\Kernel\Exceptions\Exception;
use GuzzleHttp\Exception\GuzzleException;

/**
 * 关键词管理（新版）
 */
class Client extends BaseClient
{
    /**
     * 获取关键词列表
     * 根据过滤条件获取符合条件的所有关键词。目前仅支持根据ad_id获取该计划下的关键词。
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function get($param, $token = null): array
    {
        return $this->setUri('keyword/get')->setToken($token)->request($param);
    }

    /**
     * 创建关键词
     * 在指定的ad_id下创建搜索词，可以在原有基础上进行新增。
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function create($param, $token = null): array
    {
        return $this->setUri('keyword/create_v2')->setPost()->setToken($token)->request($param);
    }

    /**
     * 更新关键词
     * 根据keyword_id更新搜索词，可批量更新。不可更改word，如需要，可通过删除接口与创建接口实现
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function update($param, $token = null): array
    {
        return $this->setUri('keyword/update_v2')->setPost()->setToken($token)->request($param);
    }

    /**
     * 删除关键词
     * 删除指定keyword_id的搜索词，可批量删除。
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function delete($param, $token = null): array
    {
        return $this->setUri('keyword/delete_v2')->setPost()->setToken($token)->request($param);
    }

    /**
     * 搜索快投关键词推荐
     * 搜索快投关键词推荐接口用于获取系统推荐的“搜索快投关键词”。
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function keywordFeedAds($param, $token = null): array
    {
        return $this->setUri('keyword_feedads/suggest')->setToken($token)->request($param);
    }
}
