<?php

namespace EasyOceanEngine\SearchLaunch\Keyword;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

/**
 * 关键词管理
 * Class ServiceProvider.
 */
class ServiceProvider implements ServiceProviderInterface
{
    /**
     * {@inheritdoc}.
     */
    public function register(Container $app)
    {
        $app['keyword'] = function ($app) {
            return new Client($app);
        };
    }
}
