<?php

namespace EasyOceanEngine\SearchLaunch\Ad;

use EasyOceanEngine\Kernel\BaseClient;
use EasyOceanEngine\Kernel\Exceptions\Exception;
use GuzzleHttp\Exception\GuzzleException;

/**
 * 广告计划
 */
class Client extends BaseClient
{
    /**
     * 创建广告计划
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function create($param, $token = null): array
    {
        return $this->setUri('ad/create')->setPost()->setToken($token)->request($param);
    }
}
