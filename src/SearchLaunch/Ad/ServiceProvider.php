<?php

namespace EasyOceanEngine\SearchLaunch\Ad;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

/**
 * 广告计划
 * Class ServiceProvider.
 */
class ServiceProvider implements ServiceProviderInterface
{
    /**
     * {@inheritdoc}.
     */
    public function register(Container $app)
    {
        $app['ad'] = function ($app) {
            return new Client($app);
        };
    }
}
