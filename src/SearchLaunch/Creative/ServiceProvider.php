<?php

namespace EasyOceanEngine\SearchLaunch\Creative;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

/**
 * 搜索广告创意模块
 *
 * Class ServiceProvider.
 */
class ServiceProvider implements ServiceProviderInterface
{
    /**
     * {@inheritdoc}.
     */
    public function register(Container $app)
    {
        $app['creative'] = function ($app) {
            return new Client($app);
        };
    }
}
