<?php

namespace EasyOceanEngine\SearchLaunch\Creative;

use EasyOceanEngine\Kernel\BaseClient;
use EasyOceanEngine\Kernel\Exceptions\Exception;
use GuzzleHttp\Exception\GuzzleException;

/**
 */
class Client extends BaseClient
{
    /**
     * 获取创意列表
     * 此接口用于获取创意列表，包括程序化创意与自定义创意；
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function get($param, $token = null): array
    {
        return $this->setUri('creative/get')->setToken($token)->request($param);
    }

    /**
     * 创建广告创意
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function create($param, $token = null): array
    {
        return $this->setUri('creative/create_v2')->setPost()->setToken($token)->request($param);
    }

    /**
     * 创意详细信息
     * 此接口用于查询一个计划下所有创意的详细信息，不包括且不支持获取计划下删除的创意；
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function read($param, $token = null): array
    {
        return $this->setUri('creative/read_v2')->setToken($token)->request($param);
    }

    /**
     * 修改创意信息
     * 此接口用于全量更新广告创意：当计划下有创意时，需要通过此接口进行全量更新与创建创意（可以先通过创意详细信息接口获取全部信息在进行更新）
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function update($param, $token = null): array
    {
        return $this->setUri('creative/update_v2')->setPost()->setToken($token)->request($param);
    }
}
