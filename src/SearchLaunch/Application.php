<?php

namespace EasyOceanEngine\SearchLaunch;

use EasyOceanEngine\Kernel\ServiceContainer;

/**
 * 搜索广告投放
 *
 * @property Campaign\Client $campaign 广告组
 * @property Ad\Client $ad 广告计划
 * @property Creative\Client $creative 广告创意
 * @property Keyword\Client $keyword 广告创意
 * @property PrivativeWord\Client $privative_word 否定词管理
 */
class Application extends ServiceContainer
{
    /**
     * @var array
     */
    protected $providers = [
        Campaign\ServiceProvider::class,
        Ad\ServiceProvider::class,
        Keyword\ServiceProvider::class,
        Creative\ServiceProvider::class,
    ];
}
