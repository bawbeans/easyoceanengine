<?php

namespace EasyOceanEngine\SearchLaunch\Campaign;

use EasyOceanEngine\Kernel\BaseClient;
use EasyOceanEngine\Kernel\Exceptions\Exception;
use GuzzleHttp\Exception\GuzzleException;

/**
 * 广告组
 */
class Client extends BaseClient
{
    /**
     * 创建广告组
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function create($param, $token = null): array
    {
        return $this->setUri('campaign/create')
            ->setPost()
            ->setToken($token)->request($param);
    }
}
