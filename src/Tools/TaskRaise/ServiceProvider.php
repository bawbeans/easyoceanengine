<?php

namespace EasyOceanEngine\Tools\TaskRaise;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

/**
 * 查询工具
 * Class ServiceProvider.
 */
class ServiceProvider implements ServiceProviderInterface
{
    /**
     * {@inheritdoc}.
     */
    public function register(Container $app)
    {
        $app['task_raise'] = function ($app) {
            return new Client($app);
        };
    }
}
