<?php

namespace EasyOceanEngine\Tools\TaskRaise;

use EasyOceanEngine\Kernel\BaseClient;
use EasyOceanEngine\Kernel\Exceptions\Exception;
use GuzzleHttp\Exception\GuzzleException;

class Client extends BaseClient
{
    /**
     * 新建优选起量任务
     *
     * 账户下生效的优选起量任务数量有且只有1个
     * 当有正在进行的优选起量任务时，不能创建新的优选起量任务
     * 当没有正在进行的优选起量任务时，只能创建1个优选起量任务
     * @throws GuzzleException
     * @throws Exception
     */
    public function create($param = [], $token = null): array
    {
        return $this->setUri('tools/task_raise/create')->setPost()->setToken($token)->request($param);
    }

    /**
     * 查询优选起量任务
     *
     * @param array $param
     * @param null $token
     * @return array
     * @throws Exception
     * @throws GuzzleException
     */
    public function get(array $param = [], $token = null): array
    {
        return $this->setUri('tools/task_raise/get')->setToken($token)->request($param);
    }

    /**
     * 关闭优选起量任务
     *
     * @param array $param
     * @param null $token
     * @return array
     * @throws Exception
     * @throws GuzzleException
     */
    public function statusStop(array $param = [], $token = null): array
    {
        return $this->setUri('tools/task_raise/status/stop/')->setPost()->setToken($token)->request($param);
    }

    /**
     * 查询优选起量状态
     *
     * @param array $param
     * @param null $token
     * @return array
     * @throws Exception
     * @throws GuzzleException
     */
    public function optimizationIdsGet(array $param = [], $token = null): array
    {
        return $this->setUri('task_raise/optimization_ids/get')->setToken($token)->request($param);
    }

    /**
     * 查询优选起量任务数据
     *
     * @param array $param
     * @param null $token
     * @return array
     * @throws Exception
     * @throws GuzzleException
     */
    public function dataGet(array $param = [], $token = null): array
    {
        return $this->setUri('tools/task_raise/data/get')->setToken($token)->request($param);
    }

}

