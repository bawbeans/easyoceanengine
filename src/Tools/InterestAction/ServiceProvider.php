<?php

namespace EasyOceanEngine\Tools\InterestAction;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

/**
 * 应用管理
 * Class ServiceProvider.
 */
class ServiceProvider implements ServiceProviderInterface
{
    /**
     * {@inheritdoc}.
     */
    public function register(Container $app)
    {
        $app['interest_action'] = function ($app) {
            return new Client($app);
        };
    }
}
