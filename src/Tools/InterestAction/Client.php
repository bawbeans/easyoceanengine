<?php

namespace EasyOceanEngine\Tools\InterestAction;

use EasyOceanEngine\Kernel\BaseClient;
use EasyOceanEngine\Kernel\Exceptions\Exception;
use GuzzleHttp\Exception\GuzzleException;

class Client extends BaseClient
{
    /**
     * 行为类目查询
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function actionCategory($param = [], $token = null): array
    {
        return $this->setUri('tools/interest_action/action/category')->setToken($token)->request($param);
    }

    /**
     * 行为关键词查询
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function actionKeyword($param = [], $token = null): array
    {
        return $this->setUri('tools/interest_action/action/keyword')->setToken($token)->request($param);
    }

    /**
     * 兴趣类目查询
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function interestCategory($param = [], $token = null): array
    {
        return $this->setUri('tools/interest_action/interest/category')->setToken($token)->request($param);
    }

    /**
     * 兴趣关键词查询
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function interestKeyword($param = [], $token = null): array
    {
        return $this->setUri('tools/interest_action/interest/keyword')->setToken($token)->request($param);
    }

    /**
     * 兴趣行为类目关键词id转词
     * @param array $param
     * @param null $token
     * @return array
     * @throws Exception
     * @throws GuzzleException
     */
    public function id2word($param = [], $token = null): array
    {
        return $this->setUri('tools/interest_action/id2word')->setToken($token)->request($param);
    }

    /**
     * 获取行为兴趣推荐关键词
     * @param array $param
     * @param null $token
     * @return array
     * @throws Exception
     * @throws GuzzleException
     */
    public function keywordSuggest($param = [], $token = null): array
    {
        return $this->setUri('tools/interest_action/keyword/suggest')->setToken($token)->request($param);
    }
}
