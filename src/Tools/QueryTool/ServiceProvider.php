<?php

namespace EasyOceanEngine\Tools\QueryTool;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

/**
 * 查询工具
 * Class ServiceProvider.
 */
class ServiceProvider implements ServiceProviderInterface
{
    /**
     * {@inheritdoc}.
     */
    public function register(Container $app)
    {
        $app['query_tool'] = function ($app) {
            return new Client($app);
        };
    }
}
