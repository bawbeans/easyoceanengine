<?php

namespace EasyOceanEngine\Tools\QueryTool;

use EasyOceanEngine\Kernel\BaseClient;
use EasyOceanEngine\Kernel\Exceptions\Exception;
use GuzzleHttp\Exception\GuzzleException;

class Client extends BaseClient
{
    /**
     * 获取行业列表,
     * 通过接口可以获取到一级行业、二级行业、三级行业列表，
     * 其中代理商创建广告主时使用的是二级行业，而在创建创意填写创意分类时使用的是三级行业，请注意区分。
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function getIndustry($param = [], $token = null): array
    {
        return $this->setUri('tools/industry/get')->setToken($token)->request($param);
    }

    /**
     * 查询应用信息
     * 如果在创建计划时你需要设置APP行为定向，那么可以使用此接口搜索对应的APP的ID，例如搜索关键词QQ将得到QQ、QQ邮箱等应用的app_id，目前仅支持android平台的搜索。
     */
    public function appSearch($param = [], $token = null): array
    {
        return $this->setUri('tools/app_search')->setToken($token)->request($param);
    }

    /**
     * 查询推广卡片推荐内容（新版）
     * 用于查询推广卡片推荐内容，可以查询的文案包括：推广卖点，卡片标题，行动号召。
     */
    public function promotion_card($param = [], $token = null): array
    {
        return $this->setUri('tools/promotion_card/recommend_title/get')->setToken($token)->request($param);
    }

    /**
     * 获取广告计划学习期状态。
     */
    public function adStatExtraInfo($param = [], $token = null): array
    {
        return $this->setUri('tools/ad_stat_extra_info/get')->setToken($token)->request($param);
    }

    /**
     * 查询在投计划配额
     */
    public function getQuota($param = [], $token = null): array
    {
        return $this->setUri('tools/quota/get/')->setToken($token)->request($param);
    }
}
