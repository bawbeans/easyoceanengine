<?php

namespace EasyOceanEngine\Tools\AudiencePackage;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

/**
 * 定向包
 * Class ServiceProvider.
 */
class ServiceProvider implements ServiceProviderInterface
{
    /**
     * {@inheritdoc}.
     */
    public function register(Container $app)
    {
        $app['audience_package'] = function ($app) {
            return new Client($app);
        };
    }
}
