<?php

namespace EasyOceanEngine\Tools\AudiencePackage;

use EasyOceanEngine\Kernel\BaseClient;
use EasyOceanEngine\Kernel\Exceptions\Exception;
use GuzzleHttp\Exception\GuzzleException;

/**
 */
class Client extends BaseClient
{
    /**
     * 获取定向包
     * 获取广告主下定向包，不同类型定向包可应用于不同类型的计划
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function get($param = [], $token = null): array
    {
        return $this->setUri('audience_package/get/')->setToken($token)->request($param);
    }

    /**
     * 创建定向包
     * 不同类型定向包可应用于不同类型的计划
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function create($param = [], $token = null): array
    {
        return $this->setUri('audience_package/create/')->setPost()->setToken($token)->request($param);
    }

    /**
     * 更新定向包
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function update($param = [], $token = null): array
    {
        return $this->setUri('audience_package/update/')->setPost()->setToken($token)->request($param);
    }

    /**
     * 更删除定向包
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function delete($param = [], $token = null): array
    {
        return $this->setUri('audience_package/delete/')->setPost()->setToken($token)->request($param);
    }

    /**
     * 计划绑定定向包
     * 创建的计划进行定向包绑定，批量接口。
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function adBind($param = [], $token = null): array
    {
        return $this->setUri('audience_package/ad/bind/')->setPost()->setToken($token)->request($param);
    }

    /**
     * 定向包解绑
     * 解绑计划绑定的定向包，批量接口
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function adUnbind($param = [], $token = null): array
    {
        return $this->setUri('audience_package/ad/unbind/')->setPost()->setToken($token)->request($param);
    }
}
