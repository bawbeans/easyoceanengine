<?php

namespace EasyOceanEngine\Tools;

use EasyOceanEngine\Kernel\ServiceContainer;
use EasyOceanEngine\Tools\Playable\ServiceProvider;

/**
 * 工具
 * @property AudiencePackage\Client $audience_package 定向包管理
 * @property AdConvert\Client $ad_convert 转化目标管理
 * @property RegionInfo\Client $region_info 查询国家/区域信息
 * @property AppManagement\Client $app_management 应用管理
 * @property AdRaise\Client $ad_raise 一键起量管理
 * @property CreativeWord\Client $creative_word 动态创意词包管理
 * @property FlowPackage\Client $flow_package 穿山甲流量包
 * @property Comment\Client $comment 评论管理
 * @property Playable\Client $playable 试玩素材管理
 * @property QueryTool\Client $query_tool 查询工具管理
 * @property InterestAction\Client $interest_action 行为兴趣词管理
 * @property TaskRaise\Client $task_raise 账户优选起量
 */
class Application extends ServiceContainer
{
    /**
     * @var array
     */
    protected $providers = [
        AudiencePackage\ServiceProvider::class,
        AdConvert\ServiceProvider::class,
        RegionInfo\ServiceProvider::class,
        AppManagement\ServiceProvider::class,
        AdRaise\ServiceProvider::class,
        CreativeWord\ServiceProvider::class,
        FlowPackage\ServiceProvider::class,
        Comment\ServiceProvider::class,
        Playable\ServiceProvider::class,
        QueryTool\ServiceProvider::class,
        InterestAction\ServiceProvider::class,
        TaskRaise\ServiceProvider::class
    ];
}
