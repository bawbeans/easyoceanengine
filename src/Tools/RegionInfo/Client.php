<?php

namespace EasyOceanEngine\Tools\RegionInfo;

use EasyOceanEngine\Kernel\BaseClient;
use EasyOceanEngine\Kernel\Exceptions\Exception;
use GuzzleHttp\Exception\GuzzleException;

class Client extends BaseClient
{
    /**
     * 获取全球国家信息
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function countryInfo($param = [], $token = null): array
    {
        return $this->setUri('tools/country/info/')->setToken($token)->request($param);
    }

    /**
     * 获取行政信息
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function adminInfo($param = [], $token = null): array
    {
        return $this->setUri('tools/admin/info/')->setToken($token)->request($param);
    }
}
