<?php

namespace EasyOceanEngine\Tools\RegionInfo;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

/**
 * 区域信息管理
 * Class ServiceProvider.
 */
class ServiceProvider implements ServiceProviderInterface
{
    /**
     * {@inheritdoc}.
     */
    public function register(Container $app)
    {
        $app['region_info'] = function ($app) {
            return new Client($app);
        };
    }
}
