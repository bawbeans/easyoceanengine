<?php

namespace EasyOceanEngine\Tools\CreativeWord;

use EasyOceanEngine\Kernel\BaseClient;
use EasyOceanEngine\Kernel\Exceptions\Exception;
use GuzzleHttp\Exception\GuzzleException;

/**
 * 动态创意词包管理
 */
class Client extends BaseClient
{
    /**
     * 创建动态创意词包
     * 创建动态创意词包：创建的词包你可以使用在创意标题中做固定词位的替换，例如创建地理位置的词包，针对用户的位置属性显示你词包中相应的地理位置词。如果相应查看可使用的词包请参考查询创意词包接口。
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function create($param = [], $token = null): array
    {
        return $this->setUri('tools/creative_word/create')->setPost()->setToken($token)->request($param);
    }

    /**
     * 查询动态创意词包
     * 创建动态创意词包：创建的词包你可以使用在创意标题中做固定词位的替换，例如创建地理位置的词包，针对用户的位置属性显示你词包中相应的地理位置词。如果相应查看可使用的词包请参考查询创意词包接口。
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function select($param = [], $token = null): array
    {
        return $this->setUri('tools/creative_word/select')->setToken($token)->request($param);
    }

    /**
     * 更新动态创意词包
     * 针对已创建的动态创意词包进行修改，可修改词包名称、默认词、替换词三部分.
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function update($param = [], $token = null): array
    {
        return $this->setUri('tools/creative_word/update')->setPost()->setToken($token)->request($param);
    }

    /**
     * 删除动态创意词包
     * 使用此接口可以删除已创建的动态创意词包。
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function delete($param = [], $token = null): array
    {
        return $this->setUri('tools/creative_word/delete')->setPost()->setToken($token)->request($param);
    }
}
