<?php

namespace EasyOceanEngine\Tools\CreativeWord;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

/**
 * 动态创意词包管理
 * Class ServiceProvider.
 */
class ServiceProvider implements ServiceProviderInterface
{
    /**
     * {@inheritdoc}.
     */
    public function register(Container $app)
    {
        $app['creative_word'] = function ($app) {
            return new Client($app);
        };
    }
}
