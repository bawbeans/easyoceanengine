<?php

namespace EasyOceanEngine\Tools\AppManagement;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

/**
 * 应用管理
 * Class ServiceProvider.
 */
class ServiceProvider implements ServiceProviderInterface
{
    /**
     * {@inheritdoc}.
     */
    public function register(Container $app)
    {
        $app['app_management'] = function ($app) {
            return new Client($app);
        };
    }
}
