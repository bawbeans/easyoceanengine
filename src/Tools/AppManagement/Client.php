<?php

namespace EasyOceanEngine\Tools\AppManagement;

use EasyOceanEngine\Kernel\BaseClient;
use EasyOceanEngine\Kernel\Exceptions\Exception;
use GuzzleHttp\Exception\GuzzleException;

class Client extends BaseClient
{
    /**
     * 查询游戏信息
     * 查询游戏预约列表接口
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function booking($param = [], $token = null): array
    {
        return $this->setUri('tools/app_management/booking/get')->setContentType()->setToken($token)->request($param);
    }

    /**
     * 查询应用信息
     * 查询应用信息 可通过请求参数advertiser_id广告主id 查询广告主下应用列表及应用详细信息
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function app($param = [], $token = null): array
    {
        return $this->setUri('tools/app_management/app/get')->setContentType()->setToken($token)->request($param);
    }
}
