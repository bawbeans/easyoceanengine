<?php

namespace EasyOceanEngine\Tools\AdRaise;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

/**
 * 一键起量管理
 * Class ServiceProvider.
 */
class ServiceProvider implements ServiceProviderInterface
{
    /**
     * {@inheritdoc}.
     */
    public function register(Container $app)
    {
        $app['ad_raise'] = function ($app) {
            return new Client($app);
        };
    }
}
