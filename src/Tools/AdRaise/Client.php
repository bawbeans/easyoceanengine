<?php

namespace EasyOceanEngine\Tools\AdRaise;

use EasyOceanEngine\Kernel\BaseClient;
use EasyOceanEngine\Kernel\Exceptions\Exception;
use GuzzleHttp\Exception\GuzzleException;

/**
 * 一键起量管理
 */
class Client extends BaseClient
{
    /**
     * 设置一键起量: 用来启动或关停一键起量服务
     *
     * @throws GuzzleException|Exception
     */
    public function set($param = [], $token = null): array
    {
        return $this->setUri('/tools/ad_raise/set')->setPost()->setToken($token)->request($param);
    }

    /**
     * 获取起量预估值
     * 获取起量预估值: 用来获取起量预估值
     *
     * @throws GuzzleException|Exception
     */
    public function estimate($param = [], $token = null): array
    {
        return $this->setUri('tools/ad_raise_estimate/get')->setToken($token)->request($param);
    }

    /**
     * 获取当前起量状态
     * 获取当前起量状态: 获取当前起量状态
     *
     * @throws GuzzleException|Exception
     */
    public function status($param = [], $token = null): array
    {
        return $this->setUri('tools/ad_raise_status/get')->setToken($token)->request($param);
    }

    /**
     * 获取一键起量的后验数据
     * 获取一键起量的后验数据: 用来获取一键起量的后验数据
     *
     * @throws GuzzleException|Exception
     */
    public function result($param = [], $token = null): array
    {
        return $this->setUri('tools/ad_raise_result/get')->setToken($token)->request($param);
    }

    /**
     * 获取一键起量报告
     * “获取一键起量报告”用于获取开启过一键起量功能的计划，在一键起量功能生效期间产生的报告数据；接口支持获取总体报告和分时报告，需要根据计划的起量版本号及起止时间进行数据获取。
     *
     * @throws GuzzleException|Exception
     */
    public function report($param = [], $token = null): array
    {
        return $this->setUri('tools/ad_raise_result/get_v2')->setToken($token)->request($param);
    }

    /**
     * 获取起量版本信息
     * “获取起量版本信息”用于获取计划在多次起量过程中产生的起量版本号及对应的起止时间。
     *
     * @throws GuzzleException|Exception
     */
    public function version($param = [], $token = null): array
    {
        return $this->setUri('tools/ad_raise_version/get')->setToken($token)->request($param);
    }
}
