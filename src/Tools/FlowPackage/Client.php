<?php

namespace EasyOceanEngine\Tools\FlowPackage;

use EasyOceanEngine\Kernel\BaseClient;
use EasyOceanEngine\Kernel\Exceptions\Exception;
use GuzzleHttp\Exception\GuzzleException;

/**
 * 穿山甲流量包
 */
class Client extends BaseClient
{
    /**
     * 创建穿山甲流量包
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function create($param = [], $token = null): array
    {
        return $this->setUri('tools/union/flow_package/create')->setPost()->setToken($token)->request($param);
    }

    /**
     * 获取穿山甲流量包
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function get($param = [], $token = null): array
    {
        return $this->setUri('tools/union/flow_package/get')->setToken($token)->request($param);
    }

    /**
     * 修改穿山甲流量包
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function update($param = [], $token = null): array
    {
        return $this->setUri('tools/union/flow_package/update')->setPost()->setToken($token)->request($param);
    }

    /**
     * 删除穿山甲流量包
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function delete($param = [], $token = null): array
    {
        return $this->setUri('tools/union/flow_package/delete')->setPost()->setToken($token)->request($param);
    }

    /**
     * 查看rit数据
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function report($param = [], $token = null): array
    {
        return $this->setUri('tools/union/flow_package/report')->setToken($token)->request($param);
    }
}
