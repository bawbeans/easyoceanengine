<?php

namespace EasyOceanEngine\Tools\FlowPackage;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

/**
 * 穿山甲流量包
 * Class ServiceProvider.
 */
class ServiceProvider implements ServiceProviderInterface
{
    /**
     * {@inheritdoc}.
     */
    public function register(Container $app)
    {
        $app['flow_package'] = function ($app) {
            return new Client($app);
        };
    }
}
