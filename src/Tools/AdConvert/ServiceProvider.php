<?php

namespace EasyOceanEngine\Tools\AdConvert;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

/**
 * 转化目标管理
 * Class ServiceProvider.
 */
class ServiceProvider implements ServiceProviderInterface
{
    /**
     * {@inheritdoc}.
     */
    public function register(Container $app)
    {
        $app['ad_convert'] = function ($app) {
            return new Client($app);
        };
    }
}
