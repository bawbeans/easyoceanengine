<?php

namespace EasyOceanEngine\Tools\AdConvert;

use EasyOceanEngine\Kernel\BaseClient;
use EasyOceanEngine\Kernel\Exceptions\Exception;
use GuzzleHttp\Exception\GuzzleException;

/**
 */
class Client extends BaseClient
{
    /**
     * 创建转化目标
     * 通过工具能力，创建一个能被计划使用的转化跟踪工具。可以通过设定转化目标（比如多少人打开应用）跟踪转化状态，衡量推广转化效果。按照转化跟踪类型目的区分目前支持两种：跟踪线索以及跟踪应用。
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function create($param = [], $token = null): array
    {
        return $this->setUri('tools/ad_convert/create/')->setPost()->setToken($token)->request($param);
    }

    /**
     * 转化目标列表
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function get($param = [], $token = null): array
    {
        return $this->setUri('/tools/adv_convert/select/')->setToken($token)->request($param);
    }

    /**
     * 查询转化目标详细信息
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function read($param = [], $token = null): array
    {
        return $this->setUri('tools/ad_convert/read/')->setToken($token)->request($param);
    }

    /**
     * 更新转化目标操作状态
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function delete($param = [], $token = null): array
    {
        return $this->setUri('tools/ad_convert/update_status/')->setPost()->setToken($token)->request($param);
    }

    /**
     * 查询广告计划可用转化目标
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function query($param = [], $token = null): array
    {
        return $this->setUri('tools/ad_convert/query/')->setToken($token)->request($param);
    }

    /**
     * 查询广告计划可用优化目标
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function getOptimizedTarget($param = [], $token = null): array
    {
        return $this->setUri('tools/ad_convert/optimized_target/get/')->setToken($token)->request($param);
    }

    /**
     * 转化目标推送
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function push($param = [], $token = null): array
    {
        return $this->setUri('tools/ad_convert/push/')->setPost()->setToken($token)->request($param);
    }

    /**
     * 查询深度优化方式
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function deepBidRead($param = [], $token = null): array
    {
        return $this->setUri('tools/ad_convert/deepbid/read/')->setToken($token)->request($param);
    }

    /**
     * 修改转化监测链接
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function trackUrlUpdate($param = [], $token = null): array
    {
        return $this->setUri('tools/ad_convert/track_url/update/')->setPost()->setToken($token)->request($param);
    }
}
