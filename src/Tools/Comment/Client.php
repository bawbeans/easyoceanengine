<?php

namespace EasyOceanEngine\Tools\Comment;

use EasyOceanEngine\Kernel\BaseClient;
use EasyOceanEngine\Kernel\Exceptions\Exception;
use GuzzleHttp\Exception\GuzzleException;

/**
 * 评论管理
 */
class Client extends BaseClient
{

    /**
     * 推送关键词
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function push($param = [], $token = null): array
    {
        return $this->setUri('tools/comment/terms_banned/add')->setPost()->setToken($token)->request($param);
    }


}
