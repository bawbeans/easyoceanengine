<?php

namespace EasyOceanEngine\Tools\Comment;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

/**
 * 评论管理
 * Class ServiceProvider.
 */
class ServiceProvider implements ServiceProviderInterface
{
    /**
     * {@inheritdoc}.
     */
    public function register(Container $app)
    {
        $app['comment'] = function ($app) {
            return new Client($app);
        };
    }
}
