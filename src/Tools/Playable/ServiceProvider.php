<?php

namespace EasyOceanEngine\Tools\Playable;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

/**
 * 转化目标管理
 * Class ServiceProvider.
 */
class ServiceProvider implements ServiceProviderInterface
{
    /**
     * {@inheritdoc}.
     */
    public function register(Container $app)
    {
        $app['playable'] = function ($app) {
            return new Client($app);
        };
    }
}
