<?php

namespace EasyOceanEngine\Tools\Playable;

use EasyOceanEngine\Kernel\BaseClient;
use EasyOceanEngine\Kernel\Exceptions\Exception;
use GuzzleHttp\Exception\GuzzleException;

/**
 * 试玩素材
 */
class Client extends BaseClient
{

    /**
     * 获取试玩素材推送结果
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function grant_result($param = [], $token = null): array
    {
        return $this->setUri('/tools/playable/grant/result')->setToken($token)->request($param);
    }


}
