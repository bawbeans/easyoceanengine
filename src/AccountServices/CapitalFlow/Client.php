<?php

namespace EasyOceanEngine\AccountServices\CapitalFlow;

use EasyOceanEngine\Kernel\BaseClient;
use EasyOceanEngine\Kernel\Exceptions\Exception;
use GuzzleHttp\Exception\GuzzleException;

/**
 * 资金和流水管理
 */
class Client extends BaseClient
{
    /**
     * 查询账号余额
     * 获取广告主或代理商账户余额信息。
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function getFund($param = [], $token = null): array
    {
        return $this->setUri('advertiser/fund/get/')->setToken($token)->request($param);
    }

    /**
     * 查询账户日流水
     * 获取广告主日流水信息，一般每天8点会出来前一天的数据，如果当天存在数据延迟可往后再尝试。
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function dailyStatFund($param = [], $token = null): array
    {
        return $this->setUri('advertiser/fund/daily_stat/')->setToken($token)->request($param);
    }

    /**
     * 查询账号流水明细
     * 获取广告主或代理商账户流水明细信息。
     * @throws GuzzleException
     * @throws Exception
     */
    public function getTransactionFund($param = [], $token = null): array
    {
        return $this->setUri('advertiser/fund/transaction/get/')->setToken($token)->request($param);
    }

    /**
     * 代理商转账
     * 通过此接口可给代理商下管理的广告主或二级代理商进行转账操作。
     * @throws GuzzleException
     * @throws Exception
     */
    public function agentRecharge($param = [], $token = null): array
    {
        return $this->setUri('agent/advertiser/recharge/')->setPost()->setToken($token)->request($param);
    }

    /**
     * 代理商退款
     * 此接口操作功能是由广告主账户将款项退回至代理商账户。
     * @throws GuzzleException
     * @throws Exception
     */
    public function agentRefund($param = [], $token = null): array
    {
        return $this->setUri('agent/advertiser/refund/')->setPost()->setToken($token)->request($param);
    }

    /**
     * 获取共享钱包余额
     * 此接口功能是查询同客户主体下共享钱包相关余额信息。
     * @throws GuzzleException
     * @throws Exception
     */
    public function sharedWalletBalance($param = [], $token = null): array
    {
        return $this->setUri('fund/shared_wallet_balance/get/')->setToken($token)->request($param);
    }


    /**
     * @param string $property
     *
     * @return mixed
     * @throws Exception
     */
    public function __get(string $property)
    {
        if (isset($this->app["capital_flow_{$property}"])) {
            return $this->app["capital_flow_{$property}"];
        }

        $class = __NAMESPACE__ . '\\' . ucfirst($property);
        if (class_exists($class)) {
            return new $class($this->app);
        }

        throw new Exception(sprintf('No capital flow qualification service named "%s".', $property));
    }
}
