<?php

namespace EasyOceanEngine\AccountServices\CapitalFlow;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

/**
 * 资金和流水管理
 *
 * Class ServiceProvider.
 */
class ServiceProvider implements ServiceProviderInterface
{
    /**
     * {@inheritdoc}.
     */
    public function register(Container $app)
    {
        $app['capital_flow'] = function ($app) {
            return new Client($app);
        };
    }
}
