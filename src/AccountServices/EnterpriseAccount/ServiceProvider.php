<?php

namespace EasyOceanEngine\AccountServices\EnterpriseAccount;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

/**
 * 企业号账号管理
 *
 * Class ServiceProvider.
 */
class ServiceProvider implements ServiceProviderInterface
{
    /**
     * {@inheritdoc}.
     */
    public function register(Container $app)
    {
        $app['enterprise_account'] = function ($app) {
            return new Client($app);
        };
    }
}
