<?php

namespace EasyOceanEngine\AccountServices\EnterpriseAccount;

use EasyOceanEngine\Kernel\BaseClient;
use EasyOceanEngine\Kernel\Exceptions\Exception;
use GuzzleHttp\Exception\GuzzleException;

/**
 * 企业号账号管理
 */
class Client extends BaseClient
{
    /**
     * 获取企业号信息
     * @throws GuzzleException
     * @throws Exception
     */
    public function info($param = [], $token = null): array
    {
        return $this->setUri('enterprise/info')->setVersion('v1.0')->setToken($token)->request($param);
    }
}
