<?php

namespace EasyOceanEngine\AccountServices\AgentAccount;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

/**
 * 代理商账号管理
 *
 * Class ServiceProvider.
 */
class ServiceProvider implements ServiceProviderInterface
{
    /**
     * {@inheritdoc}.
     */
    public function register(Container $app)
    {
        $app['agent_account'] = function ($app) {
            return new Client($app);
        };
    }
}
