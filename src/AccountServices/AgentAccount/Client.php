<?php

namespace EasyOceanEngine\AccountServices\AgentAccount;

use EasyOceanEngine\Kernel\BaseClient;
use EasyOceanEngine\Kernel\Exceptions\Exception;
use GuzzleHttp\Exception\GuzzleException;

/**
 * 代理商账号管理
 */
class Client extends BaseClient
{
    /**
     * 代理商管理账户列表
     * @throws GuzzleException
     * @throws Exception
     */
    public function advertiser($param = [], $token = null): array
    {
        return $this->setUri('agent/advertiser/select')->setToken($token)->request($param);
    }

    /**
     * 创建广告主
     * @throws GuzzleException
     * @throws Exception
     */
    public function create($param = [], $token = null): array
    {
        return $this->setUri('agent/advertiser/create_v2')->setPost()->setToken($token)->request($param);
    }

    /**
     * 修改广告主
     * @throws GuzzleException
     * @throws Exception
     */
    public function update($param = [], $token = null): array
    {
        return $this->setUri('agent/advertiser/update')->setPost()->setToken($token)->request($param);
    }

    /**
     * 获取代理商信息
     * @throws GuzzleException
     * @throws Exception
     */
    public function info($param = [], $token = null): array
    {
        return $this->setUri('agent/info')->setToken($token)->request($param);
    }

    /**
     * 二级代理商列表
     * @throws GuzzleException
     * @throws Exception
     */
    public function childAgent($param = [], $token = null): array
    {
        return $this->setUri('agent/child_agent/select')->setToken($token)->request($param);
    }
}
