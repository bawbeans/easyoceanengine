<?php

namespace EasyOceanEngine\AccountServices;

use EasyOceanEngine\Kernel\ServiceContainer;

/**
 * 资金和流水管理
 *
 * @property AdvertiserQualification\Client $advertiser_qualification
 * @property AgentAccount\Client $agent_account
 * @property EnterpriseAccount\Client $enterprise_account
 * @property CapitalFlow\Client $capital_flow
 */
class Application extends ServiceContainer
{
    /**
     * @var array
     */
    protected $providers = [
        AdvertiserQualification\ServiceProvider::class,
        AgentAccount\ServiceProvider::class,
        EnterpriseAccount\ServiceProvider::class,
        CapitalFlow\ServiceProvider::class,
    ];
}
