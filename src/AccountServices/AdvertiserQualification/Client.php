<?php

namespace EasyOceanEngine\AccountServices\AdvertiserQualification;

use EasyOceanEngine\Kernel\BaseClient;
use EasyOceanEngine\Kernel\Exceptions\Exception;
use GuzzleHttp\Exception\GuzzleException;

/**
 * 广告主信息与资质管理
 */
class Client extends BaseClient
{
    /**
     * 广告主信息
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function info($param = [], $token = null): array
    {
        return $this->setUri('advertiser/info/')->setToken($token)->request($param);
    }

    /**
     * 获取广告主资质
     * 获取广告主资质信息为全量接口，会返回广告主所有资质，注意如果广告主没有任何资质，这个接口的data将会是空。
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function getQualification($param = [], $token = null): array
    {
        return $this->setUri('advertiser/qualification/get/')->setToken($token)->request($param);
    }

    /**
     * 提交广告主资质
     * 提交广告主资质信息为全量接口。更新的时候需要全量先获取所有资质，然后更新相应资质。
     * 在开户资质中，如果资质不带id， 认为是创建一个新资质;如果将原资质删除然后提交，则认为是删除资质;如果存在资质id，则认为是更新对应资质。
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function submitQualification($param = [], $token = null): array
    {
        return $this->setUri('advertiser/qualification/submit/')
            ->setPost()
            ->setToken($token)
            ->request($param);
    }

    /**
     * 广告主公开信息
     * 获取广告主账户公开信息。
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function publicInfo($param = [], $token = null): array
    {
        return $this->setUri('advertiser/public_info/')->setToken($token)->request($param);
    }

    /**
     * 上传/更新账户头像
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function submitAvatar($param = [], $token = null): array
    {
        return $this->setUri('advertiser/avatar/submit/')
            ->setPost()
            ->setToken($token)->request($param);
    }

    /**
     * 获取广告主头像信息
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function getAvatar($param = [], $token = null): array
    {
        return $this->setUri('advertiser/avatar/get/')->setToken($token)->request($param);
    }

    /**
     * 获取投放资质信息（新版）
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function selectQualification($param = [], $token = null): array
    {
        return $this->setUri('advertiser/qualification/select_v2/')->setToken($token)->request($param);
    }

    /**
     * 批量上传投放资质
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function createQualification($param = [], $token = null): array
    {
        return $this->setUri('advertiser/qualification/create_v2/')
            ->setPost()
            ->setToken($token)->request($param);
    }
}
