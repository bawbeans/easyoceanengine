<?php

namespace EasyOceanEngine\AccountServices\AdvertiserQualification;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

/**
 * 广告主信息与资质管理
 *
 * Class ServiceProvider.
 */
class ServiceProvider implements ServiceProviderInterface
{
    /**
     * {@inheritdoc}.
     */
    public function register(Container $app)
    {
        $app['advertiser_qualification'] = function ($app) {
            return new Client($app);
        };
    }
}
