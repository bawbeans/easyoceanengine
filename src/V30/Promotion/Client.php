<?php

namespace EasyOceanEngine\V30\Promotion;

use EasyOceanEngine\Kernel\BaseClient;
use EasyOceanEngine\Kernel\Exceptions\Exception;
use GuzzleHttp\Exception\GuzzleException;

/**
 * 广告管理模块.
 */
class Client extends BaseClient
{
    /**
     * 获取广告计划
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function list($param, $token = null): array
    {
        return $this->setUri('/promotion/list/')->setVersion('v3.0')->setToken($token)->request($param);
    }

    /**
     * 修改广告计划
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function update($param, $token = null): array
    {
        return $this->setUri('promotion/update/')->setPost()->setVersion('v3.0')->setToken($token)->request($param);
    }

    /**
     * 批量获取广告审核建议
     *
     * @param $param
     * @param null $token
     * @return array
     * @throws Exception
     * @throws GuzzleException
     */
    public function rejectReason($param, $token = null): array
    {
        return $this->setUri('promotion/reject_reason/get/')->setVersion('v3.0')->setToken($token)->request($param);
    }
}

