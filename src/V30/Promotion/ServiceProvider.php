<?php

namespace EasyOceanEngine\V30\Promotion;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

/**
 * 广告管理模块
 * Class ServiceProvider.
 */
class ServiceProvider implements ServiceProviderInterface
{
    /**
     * {@inheritdoc}.
     */
    public function register(Container $app)
    {
        $app['promotion'] = function ($app) {
            return new Client($app);
        };
    }
}
