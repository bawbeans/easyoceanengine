<?php

namespace EasyOceanEngine\V30;

use EasyOceanEngine\Kernel\ServiceContainer;

/**
 * 巨量广告升级版
 *
 * @property Promotion\Client $promotion 广告管理模块
 */
class Application extends ServiceContainer
{
    /**
     * @var array
     */
    protected $providers = [
        Promotion\ServiceProvider::class,
    ];
}
