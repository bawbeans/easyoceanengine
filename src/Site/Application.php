<?php

namespace EasyOceanEngine\Site;

use EasyOceanEngine\Kernel\ServiceContainer;

/**
 * 建站管理
 * @property Orange\Client $orange 橙子建站落地页管理
 * @property ThirdSite\Client $third_site 第三方落地页管理
 * @property LandingGroup\Client $landing_group 程序化落地页管理
 */
class Application extends ServiceContainer
{
    /**
     * @var array
     */
    protected $providers = [
        Orange\ServiceProvider::class,
        ThirdSite\ServiceProvider::class,
        LandingGroup\ServiceProvider::class
    ];
}
