<?php

namespace EasyOceanEngine\Site\ThirdSite;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

/**
 * 第三方落地页管理
 * Class ServiceProvider.
 */
class ServiceProvider implements ServiceProviderInterface
{
    /**
     * {@inheritdoc}.
     */
    public function register(Container $app)
    {
        $app['third_site'] = function ($app) {
            return new Client($app);
        };
    }
}
