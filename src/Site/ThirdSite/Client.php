<?php

namespace EasyOceanEngine\Site\ThirdSite;

use EasyOceanEngine\Kernel\BaseClient;
use EasyOceanEngine\Kernel\Exceptions\Exception;
use GuzzleHttp\Exception\GuzzleException;

/**
 * 第三方落地页管理
 */
class Client extends BaseClient
{
    /**
     * 获取第三方落地页站点列表
     * 通过此接口，广告主可以获取广告主下拥有的第三方落地页站点列表。
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function get($param = [], $token = null): array
    {
        return $this->setUri('tools/third_site/get/')->setContentType()->setToken($token)->request($param);
    }

    /**
     * 创建第三方落地页站点
     * 通过此接口，用户可以创建第三方落地页站点，创建成功后接口会返回"code_0"。
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function create($param = [], $token = null): array
    {
        return $this->setUri('tools/third_site/create')->setPost()->setToken($token)->request($param);
    }

    /**
     * 修改第三方落地页站点
     * 通过此接口，用户可以修改第三方落地页站点名称name，修改成功后接口会返回"code_0"。 修改站点名称前后，站点id：site_id不变。
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function update($param = [], $token = null): array
    {
        return $this->setUri('tools/third_site/update')->setPost()->setToken($token)->request($param);
    }

    /**
     * 删除第三方落地页站点
     * 通过此接口，用户可以删除第三方落地页站点。
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function delete($param = [], $token = null): array
    {
        return $this->setUri('tools/third_site/delete')->setPost()->setToken($token)->request($param);
    }

    /**
     * 获取第三方落地页预览地址
     * 通过此接口，用户可以获取第三方落地页预览地址。
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function preview($param = [], $token = null): array
    {
        return $this->setUri('tools/third_site/preview')->setToken($token)->request($param);
    }
}
