<?php

namespace EasyOceanEngine\Site\LandingGroup;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

/**
 * 程序化落地页管理
 * Class ServiceProvider.
 */
class ServiceProvider implements ServiceProviderInterface
{
    /**
     * {@inheritdoc}.
     */
    public function register(Container $app)
    {
        $app['landing_group'] = function ($app) {
            return new Client($app);
        };
    }
}
