<?php

namespace EasyOceanEngine\Site\LandingGroup;

use EasyOceanEngine\Kernel\BaseClient;
use EasyOceanEngine\Kernel\Exceptions\Exception;
use GuzzleHttp\Exception\GuzzleException;

/**
 * 程序化落地页管理
 */
class Client extends BaseClient
{
    /**
     * 获取落地页组
     * 通过此接口，用户可以获取落地页组以及站点的基本信息。
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function get($param = [], $token = null): array
    {
        return $this->setUri('tools/landing_group/get/')->setContentType()->setToken($token)->request($param);
    }

    /**
     * 创建落地页组
     * 通过此接口，用户可以创建按照流量分配的落地页组，创建成功后，接口会返回"code_0"。
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function create($param = [], $token = null): array
    {
        return $this->setUri('tools/landing_group/create')->setPost()->setToken($token)->request($param);
    }

    /**
     * 更新落地页组站点状态
     * 通过此接口，用户可以修改落地页组站点的启用状态。
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function updateStatus($param = [], $token = null): array
    {
        return $this->setUri('tools/landing_group/site_opt_status/update')->setPost()->setToken($token)->request($param);
    }

    /**
     * 更新落地页组信息
     * 通过此接口，用户可以更新落地页组的基本信息。
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function update($param = [], $token = null): array
    {
        return $this->setUri('tools/landing_group/update')->setPost()->setToken($token)->request($param);
    }
}
