<?php

namespace EasyOceanEngine\Site\Orange;

use EasyOceanEngine\Kernel\BaseClient;
use EasyOceanEngine\Kernel\Exceptions\Exception;
use GuzzleHttp\Exception\GuzzleException;

/**
 * 橙子建站是一种在线建站工具。可以进行站点的创建，并通过页面模版、模块、组件的自由组合制作落地页，然后进行发布。
 */
class Client extends BaseClient
{
    /**
     * 创建橙子建站站点
     * 通过此接口，用户可以创建站点（用于存放落地页），之后才能创建落地页。创建站点接口会返回"code_0"，代表站点创建成功。
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function create($param = [], $token = null): array
    {
        return $this->setUri('tools/site/create/')->setPost()->setToken($token)->request($param);
    }

    /**
     * 修改橙子建站站点
     * 通过此接口，用户可以修改站点的基本信息。
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function update($param = [], $token = null): array
    {
        return $this->setUri('tools/site/update/')->setPost()->setToken($token)->request($param);
    }

    /**
     * 更改橙子建站站点状态
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function updateStatus($param = [], $token = null): array
    {
        return $this->setUri('tools/site/update_status')->setPost()->setToken($token)->request($param);
    }

    /**
     * 获取橙子建站站点预览地址
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function preview($param = [], $token = null): array
    {
        return $this->setUri('tools/site/preview/')->setContentType()->setToken($token)->request($param);
    }

    /**
     * 获取橙子建站站点详细信息
     * 通过此接口，用户可以获取站点的详细信息，包括新建或更新时传递的全量数据。
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function read($param = [], $token = null): array
    {
        return $this->setUri('tools/site/read/')->setToken($token)->request($param);
    }

    /**
     * 获取橙子建站站点列表
     * 通过此接口，用户可以获取广告主建站列表。
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function get($param = [], $token = null): array
    {
        return $this->setUri('tools/site/get/')->setContentType()->setToken($token)->request($param);
    }

    /**
     * 建站工具——查询已有智能电话
     * 通过此接口，用户可以获取已有智能电话实例列表，可用于建站。
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function getSmartPhone($param = [], $token = null): array
    {
        return $this->setUri('tools/clue/smart_phone/get/')->setContentType()->setToken($token)->request($param);
    }

    /**
     * 建站工具——查询已有表单列表
     * 通过此接口，用户可以根据广告主ID获取广告主已有表单列表，可用于建站。
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function getForm($param = [], $token = null): array
    {
        return $this->setUri('tools/clue/form/get/')->setToken($token)->request($param);
    }

    /**
     * 建站工具——查询表单详情
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function fromDetail($param = [], $token = null): array
    {
        return $this->setUri('tools/clue/form/detail')->setToken($token)->request($param);
    }

    /**
     * 获取落地页预约表单信息
     * 通过此接口，用户可以获取橙子建站落地页中的特殊的表单类型，比如附带下载类型。
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function formsList($param = [], $token = null): array
    {
        return $this->setUri('tools/site/forms/list')->setToken($token)->request($param);
    }

    /**
     * 建站工具-建站转赠
     * 通过此接口，用户可以实现站点的转赠功能，将某一广告主的站点共享给其他特定的广告主，转赠成功后，广告主们拥有站点的共同使用权。 不限制主体，同广告主不能进行转赠操作
     * 注意:用户在传入请求参数site_ids站点id列表时，请确保id的正确性，存在错误将导致转赠全部失败！
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function handsel($param = [], $token = null): array
    {
        return $this->setUri('tools/site/handsel')->setPost()->setToken($token)->request($param);
    }

    /**
     * 建站工具-建站复制
     * 通过此接口，用户可以实现站点的复制功能，成功后生成一个新站点id，站点内容和原站点一致。
     * 注意:用户在传入请求参数site_ids站点id列表时，需确保id的正确性，存在错误将导致复制全部失败！
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function copy($param = [], $token = null): array
    {
        return $this->setUri('tools/site/copy/')->setPost()->setToken($token)->request($param);
    }
}
