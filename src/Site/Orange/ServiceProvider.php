<?php

namespace EasyOceanEngine\Site\Orange;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

/**
 * 橙子建站落地页管理
 * Class ServiceProvider.
 */
class ServiceProvider implements ServiceProviderInterface
{
    /**
     * {@inheritdoc}.
     */
    public function register(Container $app)
    {
        $app['orange'] = function ($app) {
            return new Client($app);
        };
    }
}
