<?php

namespace EasyOceanEngine\DataReport\AudienceAnalysis;

use EasyOceanEngine\Kernel\BaseClient;
use EasyOceanEngine\Kernel\Exceptions\Exception;
use GuzzleHttp\Exception\GuzzleException;

/**
 * 受众分析数据报表
 */
class Client extends BaseClient
{
    /**
     * 行为兴趣数据
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function interestAction($param, $token = null): array
    {
        return $this->setUri('report/audience/interest_action/list/')->setToken($token)->request($param);
    }

    /**
     * 抖音达人数据
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function aweme($param, $token = null): array
    {
        return $this->setUri('report/audience/aweme/list')->setToken($token)->request($param);
    }

    /**
     * 省级数据
     * 获取省级受众定向数据，包括你所投放省份的总花费、展示数、点击数等数据。
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function province($param, $token = null): array
    {
        return $this->setUri('report/audience/province')->setToken($token)->request($param);
    }

    /**
     * 市级数据
     * 获取城市定向受众数据，包括你所投放城市的总花费、展示数、点击数等数据。受众分析报表数据不支持获取当天的数据。
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function city($param, $token = null): array
    {
        return $this->setUri('report/audience/city')->setToken($token)->request($param);
    }

    /**
     * 性别数据
     * 获取性别受众本数据，可以查看受众中男性和女性分别的花费和展现、点击情况数据。
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function gender($param, $token = null): array
    {
        return $this->setUri('report/audience/gender')->setToken($token)->request($param);
    }

    /**
     * 年龄数据
     * 获取年龄定向受众数据，当前包括6个年龄段（1-18；18-23；24-30；31-40；41-50；50岁以上），可以查看受众中不同年龄段用户的投放数据，包括花费、展示、点击等数据。
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function age($param, $token = null): array
    {
        return $this->setUri('report/audience/age')->setToken($token)->request($param);
    }
}
