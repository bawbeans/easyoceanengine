<?php

namespace EasyOceanEngine\DataReport\AudienceAnalysis;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

/**
 * 受众分析数据报表
 *
 * Class ServiceProvider.
 */
class ServiceProvider implements ServiceProviderInterface
{
    /**
     * {@inheritdoc}.
     */
    public function register(Container $app)
    {
        $app['audience_analysis'] = function ($app) {
            return new Client($app);
        };
    }
}
