<?php

namespace EasyOceanEngine\DataReport\AdvertisingData;

use EasyOceanEngine\Kernel\BaseClient;
use EasyOceanEngine\Kernel\Exceptions\Exception;
use GuzzleHttp\Exception\GuzzleException;

/**
 * @property Advertiser $advertiser 广告主数据
 * @property Campaign $campaign 广告组数据
 * @property Ad $ad 广告计划数据
 * @property Creative $creative 广告创意数据
 */
class Client extends BaseClient
{
    /**
     * 代理商数据
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function agent($param, $token = null): array
    {
        return $this->setUri('report/agent/get_v2/')->setToken($token)->request($param);
    }

    /**
     * 广告主数据
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function advertiser($param, $token = null): array
    {
        return $this->setUri('report/advertiser/get/')->setToken($token)->request($param);
    }

    /**
     * 广告组数据
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function campaign($param, $token = null): array
    {
        return $this->setUri('report/campaign/get/')->setToken($token)->request($param);
    }

    /**
     * 广告计划数据
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function ad($param, $token = null): array
    {
        return $this->setUri('report/ad/get/')->setToken($token)->request($param);
    }

    /**
     * 广告创意数据
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function creative($param, $token = null): array
    {
        return $this->setUri('report/creative/get/')->setToken($token)->request($param);
    }

    /**
     * 分级模糊数据
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function misty($param, $token = null): array
    {
        return $this->setUri('report/misty/get/')->setToken($token)->request($param);
    }

    /**
     * 视频素材报表
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function video($param, $token = null): array
    {
        return $this->setUri('report/video/get/')->setToken($token)->request($param);
    }

    /**
     * 视频互动流失数据
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function videoFrame($param, $token = null): array
    {
        return $this->setUri('report/video/frame/get/')->setToken($token)->request($param);
    }

    /**
     * 多合一数据报表接口
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function integrated($param, $token = null): array
    {
        return $this->setUri('report/integrated/get/')->setToken($token)->request($param);
    }
}
