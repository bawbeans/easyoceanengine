<?php

namespace EasyOceanEngine\DataReport\AdvertisingData;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

/**
 * 广告数据报表
 * Class ServiceProvider.
 */
class ServiceProvider implements ServiceProviderInterface
{
    /**
     * {@inheritdoc}.
     */
    public function register(Container $app)
    {
        $app['advertising_data'] = function ($app) {
            return new Client($app);
        };
    }
}
