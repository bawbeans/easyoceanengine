<?php

namespace EasyOceanEngine\DataReport;

use EasyOceanEngine\Kernel\ServiceContainer;

/**
 * 数据报表
 *
 * @property AdvertisingData\Client $advertising_data 广告数据报表
 * @property AudienceAnalysis\Client $audience_analysis 受众分析数据报表
 * @property AsyncData\Client $async_data 异步数据报表
 * @property OrangeStation\Client $orange_station 落地页数据报表
 */
class Application extends ServiceContainer
{
    /**
     * @var array
     */
    protected $providers = [
        AdvertisingData\ServiceProvider::class,
        AudienceAnalysis\ServiceProvider::class,
        AsyncData\ServiceProvider::class,
        OrangeStation\ServiceProvider::class,
    ];
}
