<?php

namespace EasyOceanEngine\DataReport\AsyncData;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

/**
 * 异步数据报表
 *
 * Class ServiceProvider.
 */
class ServiceProvider implements ServiceProviderInterface
{
    /**
     * {@inheritdoc}.
     */
    public function register(Container $app)
    {
        $app['async_data'] = function ($app) {
            return new Client($app);
        };
    }
}
