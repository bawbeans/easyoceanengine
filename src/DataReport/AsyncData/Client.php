<?php

namespace EasyOceanEngine\DataReport\AsyncData;

use EasyOceanEngine\Kernel\BaseClient;
use EasyOceanEngine\Kernel\Exceptions\Exception;
use GuzzleHttp\Exception\GuzzleException;

/**
 * 异步数据报表
 */
class Client extends BaseClient
{
    /**
     * 创建异步任务
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function create($param, $token = null): array
    {
        return $this->setUri('async_task/create')->setPost()->setToken($token)->request($param);
    }

    /**
     * 获取任务列表
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function get($param, $token = null): array
    {
        return $this->setUri('async_task/get')->setToken($token)->request($param);
    }

    /**
     * 下载任务结果
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function download($param, $token = null): array
    {
        return $this->setUri('async_task/download')->setToken($token)->request($param);
    }
}
