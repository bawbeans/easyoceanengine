<?php

namespace EasyOceanEngine\DataReport\OrangeStation;

use EasyOceanEngine\Kernel\BaseClient;
use EasyOceanEngine\Kernel\Exceptions\Exception;
use GuzzleHttp\Exception\GuzzleException;

/**
 * 落地页数据报表
 */
class Client extends BaseClient
{
    /**
     * 橙子建站落地页数据
     * 获取橙子建站和程序化落地页的数据。不包含第三方落地页。
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function page($param, $token = null): array
    {
        return $this->setUri('report/site/page')->setToken($token)->request($param);
    }
}
