<?php

namespace EasyOceanEngine\DataReport\OrangeStation;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

/**
 * 落地页数据报表
 *
 * Class ServiceProvider.
 */
class ServiceProvider implements ServiceProviderInterface
{
    /**
     * {@inheritdoc}.
     */
    public function register(Container $app)
    {
        $app['orange_station'] = function ($app) {
            return new Client($app);
        };
    }
}
