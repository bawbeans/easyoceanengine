<?php

namespace EasyOceanEngine\Material;

use EasyOceanEngine\Kernel\ServiceContainer;

/**
 * 素材管理
 *
 * @property Bind\Client $file_material_bind
 * @property FileImage\Client $file_image
 * @property FileVideo\Client $file_video
 */
class Application extends ServiceContainer
{
    /**
     * @var array
     */
    protected $providers = [
        FileImage\ServiceProvider::class,
        FileVideo\ServiceProvider::class,
        Bind\ServiceProvider::class,
    ];
}
