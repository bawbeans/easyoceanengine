<?php

namespace EasyOceanEngine\Material\FileVideo;

use EasyOceanEngine\Kernel\BaseClient;
use EasyOceanEngine\Kernel\Exceptions\Exception;
use EasyWeChat\Kernel\Exceptions\InvalidArgumentException;
use GuzzleHttp\Exception\GuzzleException;

/**
 *
 */
class Client extends BaseClient
{
    /**
     * 上传视频
     * 通过此接口，用户可以上传和广告相关的素材视频。
     */
    public function ad($param, $token = null)
    {
        $this->setUri('file/video/ad')->setToken($token)->setContentType('multipart/form-data');
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://ad.oceanengine.com/open_api/2/file/video/ad/');
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Content-Type: multipart/form-data',
            'Access-Token:' . $this->token,
        ]);
//        curl_setopt($ch, CURLOPT_SAFE_UPLOAD, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $param);
        $output = curl_exec($ch);
        curl_close($ch);

        $result = json_decode($output, true);
        if (empty($result)) {
            return $output;
        }

        return $result['data'];
    }

    /**
     * 获取视频素材
     * 通过此接口，用户可以获取经过一定条件过滤后的广告主下创意素材库对应的视频及视频信息。
     * @throws GuzzleException
     * @throws Exception
     */
    public function get($param, $token = null): array
    {
        return $this->setUri('file/video/get')->setToken($token)->request($param);
    }

    /**
     * 批量删除视频素材
     * 通过此接口，用户可以对素材视频进行批量删除。
     * @throws GuzzleException
     * @throws Exception
     */
    public function delete($param, $token = null): array
    {
        return $this->setUri('file/video/delete')->setPost()->setToken($token)->request($param);
    }

    /**
     * 获取视频智能封面
     * 通过此接口，用户可以获取针对素材视频推荐的智能封面。智能封面是通过提取视频关键帧筛选出推荐封面，帮助发现视频内优质封面素材。
     */
    public function coverSuggest($param, $token = null): array
    {
        return $this->setUri('tools/video_cover/suggest')->setToken($token)->request($param);
    }

    /**
     * 获取同主体下广告主视频素材
     * 通过此接口，用户可以查询同主体下的广告主视频信息。
     */
    public function adGet($param = [], $token = null): array
    {
        return $this->setUri('file/video/ad/get')->setToken($token)->request($param);
    }

    /**
     * 更新视频
     * 通过此接口，用户可以批量更新素材视频的名称。
     */
    public function update($param = [], $token = null): array
    {
        return $this->setUri('file/video/update')->setPost()->setToken($token)->request($param);
    }

    /**
     * 获取低效素材
     * 支持查询素材是否是低效素材，传入素材ID列表，返回低效素材列表。
     */
    public function getEfficiency($param = [], $token = null): array
    {
        return $this->setUri('file/video/efficiency/get')->setToken($token)->request($param);
    }
}
