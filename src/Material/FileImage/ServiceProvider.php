<?php

namespace EasyOceanEngine\Material\FileImage;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

/**
 * Class ServiceProvider.
 */
class ServiceProvider implements ServiceProviderInterface
{
    /**
     * {@inheritdoc}.
     */
    public function register(Container $app)
    {
        $app['file_image'] = function ($app) {
            return new Client($app);
        };
    }
}
