<?php

namespace EasyOceanEngine\Material\FileImage;

use EasyOceanEngine\Kernel\BaseClient;

/**
 * 素材图片操作
 */
class Client extends BaseClient
{
    /**
     * 上传广告图片
     * 同 `ad`
     */
    public function image_ad($param = [], $token = null): array
    {
        return $this->ad($param, $token);
    }

    /**
     * 上传广告图片
     * 通过此接口，用户可以上传和广告相关的素材图片，例如创意素材。
     */
    public function ad($param = [], $token = null): array
    {
        return $this->setUri('file/image/ad')->setPost()->setContentType('multipart/form-data')->setToken($token)->request($param);
    }

    /**
     * 上传广告主图片
     * 通过此接口，用户可以按照一定方式上传符合格式的广告主资质相关图片，例如营业执照等，接口会返回"code_0"和"message_OK"，代表上传成功
     */
    public function advertiser($param = [], $token = null): array
    {
        return $this->setUri('file/image/advertiser')->setPost()->setContentType('multipart/form-data')->setToken($token)->request($param);
    }

    /**
     * 获取图片素材
     * 通过此接口，用户可以获取经过一定条件过滤后的广告主下创意素材库的图片及图片信息。
     */
    public function get($param = [], $token = null): array
    {
        return $this->setUri('file/image/get')->setToken($token)->request($param);
    }

    /**
     * 获取同主体下广告主图片素材
     * 通过此接口，用户可以查询获取同主体下的广告主图片素材信息。
     */
    public function adGet($param = [], $token = null): array
    {
        return $this->setUri('file/image/ad/get')->setToken($token)->request($param);
    }
}
