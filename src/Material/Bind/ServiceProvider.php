<?php

namespace EasyOceanEngine\Material\Bind;

use EasyOceanEngine\Material\Bind\Client;
use Pimple\Container;
use Pimple\ServiceProviderInterface;

/**
 * Class ServiceProvider.
 */
class ServiceProvider implements ServiceProviderInterface
{
    /**
     * {@inheritdoc}.
     */
    public function register(Container $app)
    {
        $app['file_bind'] = function ($app) {
            return new Client($app);
        };
    }
}
