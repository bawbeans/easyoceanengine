<?php

namespace EasyOceanEngine\Material\Bind;

use EasyOceanEngine\Kernel\BaseClient;

/**
 * 素材推送
 */
class Client extends BaseClient
{
    /**
     * 素材推送
     * 通过此接口，用户可以进行同主体下不同广告主间的素材的推送。也就是说，将A广告主素材推送到，与A广告主主体（公司）相同的广告主。
     */
    public function ad($param = [], $token = null): array
    {
        return $this->setUri('file/material/bind')->setPost()->setToken($token)->request($param);
    }
}
