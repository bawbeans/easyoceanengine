<?php

namespace EasyOceanEngine\Assets\EventManager;

use EasyOceanEngine\Kernel\BaseClient;
use EasyOceanEngine\Kernel\Exceptions\Exception;
use GuzzleHttp\Exception\GuzzleException;

/**
 * 事件管理
 * 事件：关键事件是业务中实际发生的事件，一般选取用户行为漏斗中的关键事件。
 * 转化目标：以某个关键事件为转化目标。（未来会以两个事件为双目标，或者目前多转化是以多个事件为目标）
 */
class Client extends BaseClient
{
    /**
     * 获取推广内容
     * 查询广告主拥有的资产信息
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function get($param = [], $token = null): array
    {
        return $this->setUri('tools/event/assets/get/')->setToken($token)->request($param);
    }

    /**
     * 获取优化目标
     * 查询事件管理下资产的优化目标
     * todo 入参通过Params传入
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function getOptimizedGoal($param = [], $token = null): array
    {
        return $this->setUri('tools/event_convert/optimized_goal/get/')->setToken($token)->request($param);
    }

    /**
     * 创建事件
     * 此接口用于在资产下创建单个事件
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function createEvents($param = [], $token = null): array
    {
        return $this->setUri('event_manager/events/create/')->setPost()->setToken($token)->request($param);
    }

    /**
     * 创建资产
     * 此接口用于创建广告账户下资产
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function createAssets($param = [], $token = null): array
    {
        return $this->setUri('event_manager/assets/create/')->setPost()->setToken($token)->request($param);
    }

    /**
     * 获取可创建事件列表
     * 此接口用于查询资产下支持创建的事件列表。
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function getAvailableEvents($param = [], $token = null): array
    {
        return $this->setUri('event_manager/available_events/get/')->setToken($token)->request($param);
    }

    /**
     * 获取已创建事件列表
     * 此接口用于查询资产下已经创建的事件及其相关信息
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function getEventConfigs($param = [], $token = null): array
    {
        return $this->setUri('event_manager/event_configs/get/')->setToken($token)->request($param);
    }

    /**
     * 获取检测链接组信息
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function getTrackUrl($param = [], $token = null): array
    {
        return $this->setUri('event_manager/track_url/get/')->setToken($token)->request($param);
    }
    /**
     * 创建检测链接组信息
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function createTrackUrl($param = [], $token = null): array
    {
        return $this->setUri('event_manager/track_url/create/')->setPost()->setToken($token)->request($param);
    }
    /**
     * 创建检测链接组信息
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function updateTrackUrl($param = [], $token = null): array
    {
        return $this->setUri('event_manager/track_url/update/')->setPost()->setToken($token)->request($param);
    }
}
