<?php

namespace EasyOceanEngine\Assets\EventManager;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

/**
 * 事件管理
 * 事件：关键事件是业务中实际发生的事件，一般选取用户行为漏斗中的关键事件。
 * 转化目标：以某个关键事件为转化目标。（未来会以两个事件为双目标，或者目前多转化是以多个事件为目标）
 * Class ServiceProvider.
 */
class ServiceProvider implements ServiceProviderInterface
{
    /**
     * {@inheritdoc}.
     */
    public function register(Container $app)
    {
        $app['event_manager'] = function ($app) {
            return new Client($app);
        };
    }
}
