<?php

namespace EasyOceanEngine\Assets;

use EasyOceanEngine\Kernel\ServiceContainer;

/**
 * 资产
 * 目前主要资产包括创意素材相关的个人资源。
 * @property CreativeComponents\Client $creative_component 创意组件
 * @property EventManager\Client $event_manager 事件管理
 */
class Application extends ServiceContainer
{
    /**
     * @var array
     */
    protected $providers = [
        CreativeComponents\ServiceProvider::class,
        EventManager\ServiceProvider::class,
    ];
}
