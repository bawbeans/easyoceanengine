<?php

namespace EasyOceanEngine\Assets\CreativeComponents;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

/**
 * 创意组件
 * Class ServiceProvider.
 */
class ServiceProvider implements ServiceProviderInterface
{
    /**
     * {@inheritdoc}.
     */
    public function register(Container $app)
    {
        $app['creative_component'] = function ($app) {
            return new Client($app);
        };
    }
}
