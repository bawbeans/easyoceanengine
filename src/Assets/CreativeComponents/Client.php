<?php

namespace EasyOceanEngine\Assets\CreativeComponents;

use EasyOceanEngine\Kernel\BaseClient;
use EasyOceanEngine\Kernel\Exceptions\Exception;
use GuzzleHttp\Exception\GuzzleException;

/**
 * 创意组件
 * 通过通用工具能力，创建不同的创意组件，用于广告投放。
 */
class Client extends BaseClient
{
    /**
     * 查询组件列表
     * 该接口用户获取创意组件信息
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function get($param = [], $token = null): array
    {
        return $this->setUri('assets/creative_component/get/')->setToken($token)->request($param);
    }

    /**
     * 创建组件
     * 通过通用工具能力，创建不同的创意组件，用于广告投放。
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function create($param = [], $token = null): array
    {
        return $this->setUri('assets/creative_component/create')->setPost()->setToken($token)->request($param);
    }

    /**
     * 更新组件
     * 该接口用户更新创意组件信息。
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function update($param = [], $token = null): array
    {
        return $this->setUri('assets/creative_component/update')->setPost()->setToken($token)->request($param);
    }
}
