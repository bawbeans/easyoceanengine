<?php

namespace EasyOceanEngine\Launch\Ad;

use EasyOceanEngine\Kernel\BaseClient;
use EasyOceanEngine\Kernel\Exceptions\Exception;
use GuzzleHttp\Exception\GuzzleException;
use function EasyOceanEngine\Kernel\underline2camel;

/**
 * 获取广告计划
 *
 * 此接口用于获取广告计划列表的信息；
 */
class Get extends BaseClient
{
    protected $uri = 'ad/get';
    protected $method = 'GET';

    protected $page = 1;
    protected $page_size = 10;
    /**
     * @var string
     */
    protected $ad_name;
    /**
     * @var array
     */
    protected $pricing_list;
    /**
     * @var string
     */
    protected $status;
    /**
     * @var string
     */
    protected $campaign_id;
    /**
     * @var string
     */
    protected $ad_create_time;
    /**
     * @var string
     */
    protected $ad_modify_time;
    /**
     * @var string
     */
    protected $fields;

    /**
     * 按广告计划name过滤，长度为1-30个字符
     *
     * @param string $ad_name
     * @return $this
     */
    public function adName(string $ad_name)
    {
        $this->ad_name = $ad_name;
        return $this;
    }

    /**
     * 按出价方式过滤，详见【附录-计划出价类型】
     *
     * @param array $pricing_list
     * @return $this
     */
    public function pricingList(array $pricing_list)
    {
        $this->pricing_list = $pricing_list;
        return $this;
    }

    /**
     * 按计划状态过滤，默认为返回“所有不包含已删除”，如果要返回所有包含已删除有对应枚举表示，详见【附录-广告计划投放状态】
     * 注：暂不支持使用AD_STATUS_ADVERTISER_BUDGET_EXCEED筛选
     *
     * @param string $status
     * @return $this
     */
    public function status(string $status)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * 按广告组id过滤
     *
     * @param string $campaign_id
     * @return $this
     */
    public function campaignId(string $campaign_id)
    {
        $this->campaign_id = $campaign_id;
        return $this;
    }

    /**
     * 广告计划创建时间，格式yyyy-mm-dd，表示过滤出当天创建的广告计划
     *
     * @param string $ad_create_time
     * @return $this
     */
    public function adCreateTime(string $ad_create_time)
    {
        $this->ad_create_time = $ad_create_time;
        return $this;
    }

    /**
     * 广告计划更新时间，格式yyyy-mm-dd，表示过滤出当天更新的广告计划
     *
     * @param string $ad_modify_time
     * @return $this
     */
    public function adModifyTime(string $ad_modify_time)
    {
        $this->ad_modify_time = $ad_modify_time;
        return $this;
    }

    /**
     * 查询字段集合, 如果指定, 则返回结果数组中，不支持audience下字段筛选
     *
     * @param string $fields
     * @return $this
     */
    public function fields(string $fields)
    {
        $this->fields = $fields;
        return $this;
    }

    /**
     * @throws Exception
     */
    public function filtering($field, $value)
    {
        if (!in_array($field, ['ids', 'ad_name', 'pricing_list', 'status', 'campaign_id', 'ad_create_time', 'ad_modify_time'])) {
            throw new Exception('不支持' . $field . '参数', 100);
        }
        $func = underline2camel($field);
        $this->$func($value);
        return $this;
    }

    /**
     * 当前页码
     *
     * @param $page
     * @return $this
     */
    public function page($page)
    {
        $this->page = $page;
        return $this;
    }

    /**
     * 当前页码
     *
     * @param $page_size
     * @return $this
     */
    public function pageSize($page_size)
    {
        $this->page_size = $page_size;
        return $this;
    }

    /**
     * 执行请求
     *
     * @return array
     * @throws Exception
     * @throws GuzzleException
     */
    public function send()
    {
        $option = [
            'advertiser_id' => $this->advertiser_id,
            'fields' => $this->fields,
            'page' => $this->page,
            'page_size' => $this->page_size,
        ];
        $filtering = [];
        empty($this->ids) || $filtering['ids'] = $this->ids;
        empty($this->ad_name) || $filtering['ad_name'] = $this->ad_name;
        empty($this->pricing_list) || $filtering['pricing_list'] = $this->pricing_list;
        empty($this->status) || $filtering['status'] = $this->status;
        empty($this->campaign_id) || $filtering['campaign_id'] = $this->campaign_id;
        empty($this->ad_create_time) || $filtering['ad_create_time'] = $this->ad_create_time;
        empty($this->ad_modify_time) || $filtering['ad_modify_time'] = $this->ad_modify_time;
        empty($filtering) || $option['filtering'] = $filtering;

        return $this->request($option);
    }
}
