<?php

namespace EasyOceanEngine\Launch\Ad;

use EasyOceanEngine\Kernel\BaseClient;
use EasyOceanEngine\Kernel\Exceptions\Exception;
use GuzzleHttp\Exception\GuzzleException;

/**
 * @property Get $get 获取广告计划
 */
class Client extends BaseClient
{
    /**
     * 获取广告计划
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function get($param, $token = null): array
    {
        return $this->setUri('ad/get/')->setToken($token)->request($param);
    }

    /**
     * 获取广告计划（营销链路）
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function create($param, $token = null): array
    {
        return $this->setUri('ad/create/')->setPost()->setToken($token)->request($param);
    }

    /**
     * 修改广告计划（营销链路）
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function update($param, $token = null): array
    {
        return $this->setUri('ad/update/')->setPost()->setToken($token)->request($param);
    }

    /**
     * 更新计划状态
     * 通过此接口可对计划做启用/暂停/删除操作；
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function updateStatus($param, $token = null): array
    {
        return $this->setUri('ad/update/status/')->setPost()->setToken($token)->request($param);
    }

    /**
     * 更新计划预算
     * 通过此接口用于更新广告计划的预算；
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function updateBudget($param, $token = null): array
    {
        return $this->setUri('ad/update/budget/')->setPost()->setToken($token)->request($param);
    }

    /**
     * 更新计划出价
     * 通过此接口用于更新广告计划的出价 bid， 暂不支持更新深度出价
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function updateBid($param, $token = null): array
    {
        return $this->setUri('ad/update/bid/')->setPost()->setToken($token)->request($param);
    }

    /**
     * 获取计划审核建议
     * 通过此接口用于更新广告计划纬度的审核意见；
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function rejectReason($param, $token = null): array
    {
        return $this->setUri('ad/reject_reason/')->setToken($token)->request($param);
    }

    /**
     * 批量获取计划成本保障状态
     * 通过此接口用于更新广告计划纬度的审核意见；
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function getCostProtectStatus($param, $token = null): array
    {
        return $this->setUri('ad/cost_protect_status/get/')->setContentType()->setToken($token)->request($param);
    }

    /**
     * @param string $property
     *
     * @return mixed
     *
     * @throws Exception
     */
    public function __get(string $property)
    {
        if (isset($this->app["ad_{$property}"])) {
            return $this->app["ad_{$property}"];
        }

        $class = __NAMESPACE__ . '\\' . ucfirst($property);
        if (class_exists($class)) {
            return new $class($this->app);
        }

        throw new Exception(sprintf('No ad service named "%s".', $property));
    }
}
