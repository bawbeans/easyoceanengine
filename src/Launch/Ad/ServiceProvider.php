<?php

namespace EasyOceanEngine\Launch\Ad;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

/**
 * 广告计划模块
 * Class ServiceProvider.
 */
class ServiceProvider implements ServiceProviderInterface
{
    /**
     * {@inheritdoc}.
     */
    public function register(Container $app)
    {
        $app['ad'] = function ($app) {
            return new Client($app);
        };
        $app['ad_get'] = function ($app) {
            return new Get($app);
        };
    }
}
