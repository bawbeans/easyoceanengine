<?php

namespace EasyOceanEngine\Launch\Creative;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

/**
 * 广告创意模块
 *
 * Class ServiceProvider.
 */
class ServiceProvider implements ServiceProviderInterface
{
    /**
     * {@inheritdoc}.
     */
    public function register(Container $app)
    {
        $app['creative'] = function ($app) {
            return new Client($app);
        };
        $app['creative_get'] = function ($app) {
            return new Get($app);
        };
    }
}
