<?php

namespace EasyOceanEngine\Launch\Creative;

use EasyOceanEngine\Kernel\BaseClient;
use EasyOceanEngine\Kernel\Exceptions\Exception;
use GuzzleHttp\Exception\GuzzleException;

/**
 * @property Get $get 获取广告创意
 */
class Client extends BaseClient
{
    /**
     * 获取创意列表
     * 此接口用于获取创意列表，包括程序化创意与自定义创意；
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function get($param, $token = null): array
    {
        return $this->setUri('creative/get/')->setToken($token)->request($param);
    }

    /**
     * 创建自定义创意（营销链路）
     * 此接口用于创建自定义广告创意
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function createCustom($param, $token = null): array
    {
        return $this->setUri('creative/custom_creative/create/')->setPost()->setToken($token)->request($param);
    }

    /**
     * 修改自定义创意（营销链路）
     * 此接口用于全量更新自定义广告创意
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function updateCustom($param, $token = null): array
    {
        return $this->setUri('creative/custom_creative/update/')->setPost()->setToken($token)->request($param);
    }

    /**
     * 创建程序化创意（营销链路）
     * 此接口用于创建程序化广告创意
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function createProcedural($param, $token = null): array
    {
        return $this->setUri('creative/procedural_creative/create/')->setPost()->setToken($token)->request($param);
    }

    /**
     * 修改程序化创意（营销链路）
     * 此接口用于全量更新广告程序化创意： 当计划下有创意时，需要通过此接口进行全量更新与创建创意（可以先通过创意详细信息接口获取全部信息在进行更新）
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function updateProcedural($param, $token = null): array
    {
        return $this->setUri('creative/procedural_creative/update/')->setPost()->setToken($token)->request($param);
    }

    /**
     * 创建广告创意
     * 此接口用于创建广告创意，对于搜索广告的创建可参照【搜索广告投放】
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function create($param, $token = null): array
    {
        return $this->setUri('creative/create_v2/')->setPost()->setToken($token)->request($param);
    }

    /**
     * 创意详细信息
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function info($param, $token = null): array
    {
        return $this->setUri('creative/read_v2/')->setToken($token)->request($param);
    }

    /**
     * 修改创意信息
     * 此接口用于全量更新广告创意：当计划下有创意时，需要通过此接口进行全量更新与创建创意（可以先通过创意详细信息接口获取全部信息在进行更新）
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function update($param, $token = null): array
    {
        return $this->setUri('creative/update_v2')->setPost()->setToken($token)->request($param);
    }

    /**
     * 更新创意状态
     * 此接口用于批量修改创意状态
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function updateStatus($param, $token = null): array
    {
        return $this->setUri('creative/status/update_v2/')->setPost()->setToken($token)->request($param);
    }

    /**
     * 创意素材信息
     * 此接口用于查询一个创意下的素材信息，包括图片/视频、标题；
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function materialInfo($param, $token = null): array
    {
        return $this->setUri('creative/material/read/')->setPost()->setToken($token)->request($param);
    }

    /**
     * 获取创意审核建议
     * 此接口用于获取创意审核建议；
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function rejectReason($param, $token = null): array
    {
        return $this->setUri('creative/reject_reason/')->setToken($token)->request($param);
    }

    /**
     * @param string $property
     *
     * @return mixed
     *
     * @throws Exception
     */
    public function __get(string $property)
    {
        if (isset($this->app["ad_{$property}"])) {
            return $this->app["ad_{$property}"];
        }

        $class = __NAMESPACE__ . '\\' . ucfirst($property);
        if (class_exists($class)) {
            return new $class($this->app);
        }

        throw new Exception(sprintf('No creative service named "%s".', $property));
    }
}
