<?php

namespace EasyOceanEngine\Launch\Creative;

use EasyOceanEngine\Kernel\BaseClient;
use EasyOceanEngine\Kernel\Exceptions\Exception;
use GuzzleHttp\Exception\GuzzleException;
use function EasyOceanEngine\Kernel\underline2camel;

/**
 * 获取创意列表
 *
 * 此接口用于获取创意列表，包括程序化创意与自定义创意；
 */
class Get extends BaseClient
{
    protected $uri = 'creative/get';
    protected $method = 'GET';

    protected $page = 1;
    protected $page_size = 10;

    /**
     * @var string
     */
    protected $fields;
    /**
     * @var string
     */
    protected $campaign_id;
    /**
     * @var int|string
     */
    protected $ad_id;
    /**
     * @var array
     */
    protected $creative_ids;
    /**
     * @var string
     */
    protected $creative_title;
    /**
     * @var string
     */
    protected $landing_type;
    /**
     * @var string
     */
    protected $pricing;
    /**
     * @var string
     */
    protected $status;
    /**
     * @var string
     */
    protected $image_mode;
    /**
     * @var string
     */
    protected $creative_create_time;
    /**
     * @var string
     */
    protected $creative_modify_time;

    /**
     * 按照campaign_id过滤
     *
     * @param string $campaign_id
     * @return $this
     */
    public function campaignId(string $campaign_id)
    {
        $this->campaign_id = $campaign_id;
        return $this;
    }

    /**
     * 按出价方式过滤，详见【附录-计划出价类型】
     *
     * @param string|integer $ad_id
     * @return $this
     */
    public function adId($ad_id)
    {
        $this->ad_id = $ad_id;
        return $this;
    }

    /**
     * 按照creative_id过滤，最多传100个。创意ID需属于当前广告主，否则会报错
     *
     * @param array $creative_ids
     * @return $this
     */
    public function creativeIds(array $creative_ids)
    {
        $this->creative_ids = $creative_ids;
        return $this;
    }

    /**
     * 按照creative_title过滤，支持模糊搜索。支持的最大长度为30
     *
     * @param string $creative_title
     * @return $this
     */
    public function creativeTitle(string $creative_title)
    {
        $this->creative_title = $creative_title;
        return $this;
    }

    /**
     * 按照广告组推广目的过滤，【附录-推广目的类型】
     *
     * @param string $landing_type
     * @return $this
     */
    public function landing_type(string $landing_type)
    {
        $this->landing_type = $landing_type;
        return $this;
    }

    /**
     * 按照广告计划出价方式过滤，【附录-计划出价类型】
     *
     * @param string $pricing
     * @return $this
     */
    public function pricing(string $pricing)
    {
        $this->pricing = $pricing;
        return $this;
    }

    /**
     * 按照创意状态过滤，默认为返回“所有不包含已删除”，如果要返回所有包含已删除有对应枚举表示，【附录-广告创意状态】
     *
     * @param string status
     * @return $this
     */
    public function status(string $status)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * 按照创意素材类型过滤，【附录-素材类型】
     *
     * @param string $image_mode
     * @return $this
     */
    public function imageMode(string $image_mode)
    {
        $this->image_mode = $image_mode;
        return $this;
    }

    /**
     * 广告创意创建时间，格式yyyy-MM-dd，表示过滤出当天创建的广告创意
     *
     * @param string $creative_create_time
     * @return $this
     */
    public function creativeCreateTime(string $creative_create_time)
    {
        $this->creative_create_time = $creative_create_time;
        return $this;
    }

    /**
     * 广告创意创建时间，格式yyyy-MM-dd，表示过滤出当天创建的广告创意
     *
     * @param string $creative_modify_time
     * @return $this
     */
    public function creativeModifyTime(string $creative_modify_time)
    {
        $this->creative_modify_time = $creative_modify_time;
        return $this;
    }

    /**
     * 广告创意创建时间，格式yyyy-MM-dd，表示过滤出当天创建的广告创意
     *
     * @param array $fields
     * @return $this
     */
    public function fields(array $fields)
    {
//        $field = ["creative_id", "ad_id", "advertiser_id", "status", "opt_status", "image_mode", "title", "creative_word_ids", "third_party_id", "image_ids", "image_id", "video_id", "materials"];
        $this->fields = $fields;
        return $this;
    }

    /**
     * @throws Exception
     */
    public function filtering($field, $value)
    {
        if (!in_array($field, ['ids', 'ad_name', 'pricing_list', 'status', 'campaign_id', 'creative_create_time', 'ad_modify_time'])) {
            throw new Exception('不支持' . $field . '参数', 100);
        }
        $func = underline2camel($field);
        $this->$func($value);
        return $this;
    }

    /**
     * 当前页码
     *
     * @param $page
     * @return $this
     */
    public function page($page)
    {
        $this->page = $page;
        return $this;
    }

    /**
     * 当前页码
     *
     * @param $page_size
     * @return $this
     */
    public function pageSize($page_size)
    {
        $this->page_size = $page_size;
        return $this;
    }

    /**
     * 执行请求
     *
     * @return array
     * @throws Exception
     * @throws GuzzleException
     */
    public function send()
    {
        $option = [
            'advertiser_id' => $this->advertiser_id,
            'fields' => $this->fields,
            'page' => $this->page,
            'page_size' => $this->page_size,
        ];
        $filtering = [];
        empty($this->campaign_id) || $filtering['campaign_id'] = $this->campaign_id;
        empty($this->ad_id) || $filtering['ad_id'] = $this->ad_id;
        empty($this->creative_ids) || $filtering['creative_ids'] = $this->creative_ids;
        empty($this->creative_title) || $filtering['creative_title'] = $this->creative_title;
        empty($this->landing_type) || $filtering['landing_type'] = $this->landing_type;
        empty($this->pricing) || $filtering['pricing'] = $this->pricing;
        empty($this->status) || $filtering['status'] = $this->status;
        empty($this->image_mode) || $filtering['image_mode'] = $this->image_mode;
        empty($this->creative_create_time) || $filtering['creative_create_time'] = $this->creative_create_time;
        empty($this->creative_modify_time) || $filtering['creative_modify_time'] = $this->creative_modify_time;
        empty($filtering) || $option['filtering'] = $filtering;

        return $this->request($option);
    }
}
