<?php

namespace EasyOceanEngine\Launch\Campaign;

use EasyOceanEngine\Kernel\BaseClient;
use EasyOceanEngine\Kernel\Exceptions\Exception;
use GuzzleHttp\Exception\GuzzleException;

/**
 * 广告组更新状态
 *
 * 此接口用于更新广告组的状态；
 */
class UpdateStatus extends BaseClient
{
    protected $uri = 'campaign/update/status/';
    protected $method = 'POST';
    protected $content_type = 'application/json';

    /**
     * 广告组ID，不超过100个，且广告组ID属于广告主ID否则会报错；
     * @var array
     */
    protected $campaign_ids;

    /**
     * @var string
     */
    protected $opt_status;

    /**
     * 广告组ID，广告组ID需要属于广告主ID，否则会报错！
     *
     * @param array $campaign_ids
     * @return $this
     */
    public function campaignIds(array $campaign_ids)
    {
        $this->campaign_ids = $campaign_ids;
        return $this;
    }

    /**
     * 操作, "enable"表示启用, "delete"表示删除, "disable"表示暂停；
     * 允许值: "enable", "delete", "disable",
     * 对于删除的广告组不可进行任何操作，否则会报错哈
     *
     * @param string $opt_status
     * @return $this
     */
    public function optStatus(string $opt_status)
    {
        $this->opt_status = $opt_status;
        return $this;
    }

    /**
     * 执行请求
     *
     * @return array
     * @throws Exception
     * @throws GuzzleException
     */
    public function send()
    {
        $option = [
            'advertiser_id' => $this->advertiser_id,
            'campaign_ids' => $this->campaign_ids,
            'opt_status' => $this->opt_status,
        ];

        return $this->request($option);
    }
}
