<?php

namespace EasyOceanEngine\Launch\Campaign;

use EasyOceanEngine\Kernel\BaseClient;
use EasyOceanEngine\Kernel\Exceptions\Exception;
use GuzzleHttp\Exception\GuzzleException;

/**
 * 创建广告组
 *
 * 创建新营销链路下广告组，需要选择营销目的marketing_purpose，传入营销目的下对应的推广目的landing_type，设定其他基础信息（广告类型，预算，名称等）完成创建
 */
class Create extends BaseClient
{
    protected $uri = 'campaign/create/';
    protected $method = 'POST';

    public $campaign_name;
    /**
     * @var string
     */
    public $operation = 'enable';
    /**
     * @var string
     */
    public $budget_mode;
    /**
     * @var string
     */
    public $budget;
    /**
     * @var string
     */
    public $landing_type;
    /**
     * @var string
     */
    public $campaign_type;
    /**
     * @var string
     */
    public $marketing_purpose;
    /**
     * @var string
     */
    public $delivery_related_num;

    /**
     * 广告组名称，长度为1-100个字符，其中1个中文字符算2位
     * @param string $campaign_name
     * @return $this
     */
    public function campaignName(string $campaign_name)
    {
        $this->campaign_name = $campaign_name;
        return $this;
    }

    /**
     * 广告组状态
     *
     * 允许值: enable,disable默认值：enable开启状态
     * @param string $operation
     * @return $this
     */
    public function operation(string $operation)
    {
        $this->operation = $operation;
        return $this;
    }

    /**
     * 广告组预算类型
     *
     * 允许值:BUDGET_MODE_INFINITE,BUDGET_MODE_DAY
     * @param string $budget_mode
     * @return $this
     */
    public function budgetMode(string $budget_mode)
    {
        $this->budget_mode = $budget_mode;
        return $this;
    }

    /**
     * 广告组预算，取值范围: ≥ 0
     *
     * 当budget_mode为"BUDGET_MODE_DAY"时,必填,且日预算不少于300元
     * @param string $budget
     * @return $this
     */
    public function budget(string $budget)
    {
        $this->budget = $budget;
        return $this;
    }

    /**
     * 广告组推广目的, 详见【附录-推广目的类型】(创建后不可修改)
     * 允许值: LINK、APP、QUICK_APP、DPA、GOODS、SHOP、AWEME、LIVE
     * @param string $landing_type
     * @return $this
     */
    public function landingType(string $landing_type)
    {
        $this->landing_type = $landing_type;
        return $this;
    }

    /**
     * 广告组类型，允许值FEED信息流、SEARCH搜索广告
     * @param string $campaign_type
     * @return $this
     */
    public function campaignType(string $campaign_type)
    {
        $this->campaign_type = $campaign_type;
        return $this;
    }


    /**
     * 营销目的，允许值CONVERSION行动转化、INTENTION用户意向、ACKNOWLEDGE品牌认知
     * 创建新版营销链路广告组，该字段必填
     * @param string $marketing_purpose
     * @return $this
     */
    public function marketingPurpose(string $marketing_purpose)
    {
        $this->marketing_purpose = $marketing_purpose;
        return $this;
    }

    /**
     * 广告组商品类型，详见【附录-广告组商品类型】(创建后不可修改)
     * 允许值:
     * CAMPAIGN_DPA_DEFAULT_NOT: 非 DPA
     * CAMPAIGN_DPA_SINGLE_DELIVERY:SDPA 单商品推广
     * CAMPAIGN_DPA_MULTI_DELIVERY: DPA 商品推广
     * SDPA单商品推广除商品目录推广外还支持应用推广（APP）、销售线索收集(LINK)、快应用(QUICK_APP)和电商店铺(SHOP)推广目的。
     * 默认值：
     * 推广目的非 DPA 时默认 CAMPAIGN_DPA_DEFAULT_NOT
     * 推广目的为 DPA 时默认 CAMPAIGN_DPA_MULTI_DELIVERY
     * @param string $delivery_related_num
     * @return $this
     */
    public function deliveryRelatedNum(string $delivery_related_num)
    {
        $this->delivery_related_num = $delivery_related_num;
        return $this;
    }


    /**
     * 执行请求
     *
     * @return array
     * @throws Exception
     * @throws GuzzleException
     */
    public function send()
    {
        $option = [
            'advertiser_id' => $this->advertiser_id,
            'campaign_name' => $this->campaign_name,
            'operation' => $this->operation,
            'budget_model' => $this->budget_mode,
            'landing_type' => $this->landing_type,
        ];
        empty($this->budget) || $option['budget'] = $this->budget;
        empty($this->campaign_type) || $option['campaign_type'] = $this->campaign_type;
        empty($this->marketing_purpose) || $option['marketing_purpose'] = $this->marketing_purpose;
        empty($this->delivery_related_num) || $option['delivery_related_num'] = $this->delivery_related_num;

        return $this->request($option);
    }
}
