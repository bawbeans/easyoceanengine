<?php

namespace EasyOceanEngine\Launch\Campaign;

use EasyOceanEngine\Kernel\BaseClient;
use EasyOceanEngine\Kernel\Exceptions\Exception;
use GuzzleHttp\Exception\GuzzleException;

/**
 * @property Create $create 创建
 * @property Get $get 获取
 * @property Update $update 更新
 * @property UpdateStatus $update_status 更新
 */
class Client extends BaseClient
{
    /**
     * 获取广告组
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function get($param, $token = null): array
    {
        return $this->setUri('campaign/get/')->setToken($token)->request($param);
    }

    /**
     * 创建广告组
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function create($param, $token = null): array
    {
        return $this->setUri('campaign/create')
            ->setPost()
            ->setToken($token)->request($param);
    }

    /**
     * 修改广告组
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function update($param, $token = null): array
    {
        return $this->setUri('campaign/update')
            ->setPost()
            ->setToken($token)->request($param);
    }

    /**
     * 修改广告组
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function updateStatus($param, $token = null): array
    {
        return $this->setUri('campaign/update/status/')
            ->setPost()
            ->setToken($token)->request($param);
    }

    /**
     * @param string $property
     *
     * @return mixed
     *
     * @throws Exception
     */
    public function __get($property)
    {
        if (isset($this->app["campaign_{$property}"])) {
            return $this->app["campaign_{$property}"];
        }

        $class = __NAMESPACE__ . '\\' . ucfirst($property);
        if (class_exists($class)) {
            return new $class($this->app);
        }

        throw new Exception(sprintf('No campaign service named "%s".', $property));
    }
}
