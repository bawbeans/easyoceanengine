<?php

namespace EasyOceanEngine\Launch\Campaign;

use EasyOceanEngine\Kernel\BaseClient;
use EasyOceanEngine\Kernel\Exceptions\Exception;
use GuzzleHttp\Exception\GuzzleException;
use function EasyOceanEngine\Kernel\underline2camel;

/**
 * 获取广告组
 *
 * 支持filtering过滤，可按广告组ID、推广目的、广告组状态进行过滤筛选，默认不获取删除的广告组，如果要获取删除的广告组，可在filtering中传入对应的status值
 */
class Get extends BaseClient
{
    protected $uri = 'campaign/get/';
    protected $method = 'GET';

    protected $page = 1;
    protected $page_size = 10;

    /**
     * 查询字段集合，如果指定，则返回结果数组中， 每个元素是包含所查询字段的字典
     * @var string
     */
    protected $fields = ['id', 'name', 'budget', ' budget_mode', 'landing_type', 'status', 'modify_time', 'status', 'modify_time', 'campaign_modify_time', 'campaign_create_time'];
    /**
     * @var string
     */
    protected $landing_type;
    /**
     * @var array
     */
    protected $ids;
    /**
     * @var string
     */
    protected $campaign_name;
    /**
     * @var string
     */
    protected $status;
    /**
     * @var string
     */
    protected $campaign_create_time;

    /**
     * 查询字段集合，如果指定，则返回结果数组中， 每个元素是包含所查询字段的字典
     *
     * @param array $fields
     * @return $this
     */
    public function fields(array $fields)
    {
        $this->fields = $fields;
        return $this;
    }

    /**
     * 广告组ID过滤，数组，不超过100个
     *
     * @param array $ids
     * @return $this
     */
    public function ids(array $ids)
    {
        $this->ids = $ids;
        return $this;
    }

    /**
     * 广告组name过滤，长度为1-30个字符，其中1个中文字符算2位
     *
     * @param string $campaign_name
     * @return $this
     */
    public function campaignName(string $campaign_name)
    {
        $this->campaign_name = $campaign_name;
        return $this;
    }

    /**
     * 广告组推广目的过滤，详见【附录-推广目的类型】
     *
     * @param string $landing_type
     * @return $this
     */
    public function landingType(string $landing_type)
    {
        $this->landing_type = $landing_type;
        return $this;
    }

    /**
     * 广告组推广目的过滤，详见【附录-推广目的类型】
     *
     * @param string $landing_type
     * @return $this
     */
    public function status(string $status)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * 广告组创建时间，格式yyyy-mm-dd，表示过滤出当天创建的广告组
     *
     * @param string $landing_type
     * @return $this
     */
    public function campaignCreateTime(string $campaign_create_time)
    {
        $this->campaign_create_time = $campaign_create_time;
        return $this;
    }

    public function filtering($field, $value)
    {
        if (!in_array($field, ['ids', 'campaign_name', 'landing_type', 'status', 'campaign_create_time'])) {
            throw new Exception('不支持' . $field . '参数', 100);
        }
        $func = underline2camel($field);
        $this->$func($value);
        return $this;
    }

    /**
     * 当前页码
     *
     * @param $page
     * @return $this
     */
    public function page($page)
    {
        $this->page = $page;
        return $this;
    }

    /**
     * 当前页码
     *
     * @param $page_size
     * @return $this
     */
    public function pageSize($page_size)
    {
        $this->page_size = $page_size;
        return $this;
    }

    /**
     * 执行请求
     *
     * @return array
     * @throws Exception
     * @throws GuzzleException
     */
    public function send()
    {
        $option = [
            'advertiser_id' => $this->advertiser_id,
            'fields' => $this->fields,
            'page' => $this->page,
            'page_size' => $this->page_size,
        ];
        empty($this->ids) || $option['ids'] = $this->ids;
        empty($this->campaign_name) || $option['campaign_name'] = $this->campaign_name;
        empty($this->landing_type) || $option['landing_type'] = $this->landing_type;
        empty($this->status) || $option['status'] = $this->status;
        empty($this->campaign_create_time) || $option['campaign_create_time'] = $this->campaign_create_time;

        return $this->request($option);
    }
}
