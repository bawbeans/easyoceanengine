<?php

namespace EasyOceanEngine\Launch\Campaign;

use EasyOceanEngine\Kernel\BaseClient;
use EasyOceanEngine\Kernel\Exceptions\Exception;
use GuzzleHttp\Exception\GuzzleException;

/**
 * 修改广告组
 *
 * 可通过当前接口修改广告组名称、预算类型以及预算（推广目的一旦创建是不允许修改的）
 */
class Update extends BaseClient
{
    protected $uri = 'campaign/update/';
    protected $method = 'POST';
    protected $content_type = 'application/json';

    /**
     * @var string
     */
    protected $campaign_name;
    /**
     * @var string
     */
    protected $budget_mode;
    /**
     * @var string
     */
    protected $budget;

    /**
     * 广告组ID，广告组ID需要属于广告主ID，否则会报错！
     * @var string
     */
    protected $campaign_id;
    /**
     * @var string
     */
    protected $modify_time;

    /**
     * 广告组名称，长度为1-100个字符，其中1个中文字符算2位
     * @param string $campaign_name
     * @return $this
     */
    public function campaignName(string $campaign_name)
    {
        $this->campaign_name = $campaign_name;
        return $this;
    }

    /**
     * 广告组预算类型, 详见【附录-预算类型】
     * 允许值:"BUDGET_MODE_INFINITE","BUDGET_MODE_DAY"
     *
     * @param string $budget_mode
     * @return $this
     */
    public function budgetMode(string $budget_mode)
    {
        $this->budget_mode = $budget_mode;
        return $this;
    }

    /**
     * 广告组预算(当budget_mode=BUDGET_MODE_DAY时,广告组日预算不能低于300元,预算单次修改幅度不能低于100元)取值范围: ≥ 0，更详细的要求可以查看文档上方介绍；
     *
     * @param string $budget
     * @return $this
     */
    public function budget(string $budget)
    {
        $this->budget = $budget;
        return $this;
    }

    /**
     * 广告组ID，广告组ID需要属于广告主ID，否则会报错！
     *
     * @param string $campaign_id
     * @return $this
     */
    public function campaignId(string $campaign_id)
    {
        $this->campaign_id = $campaign_id;
        return $this;
    }

    /**
     * 时间戳（从campaign/get/接口得到,用于判断是否基于最新信息修改，正确获取与填写，以免报错！)
     *
     * @param string $modify_time
     * @return $this
     */
    public function modifyTime(string $modify_time)
    {
        $this->modify_time = $modify_time;
        return $this;
    }

    /**
     * 执行请求
     *
     * @return array
     * @throws Exception
     * @throws GuzzleException
     */
    public function send()
    {
        $option = [
            'advertiser_id' => $this->advertiser_id,
            'campaign_id' => $this->campaign_name,
            'modify_time' => $this->modify_time,
        ];
        empty($this->campaign_name) || $option['campaign_name'] = $this->campaign_name;
        empty($this->budget_mode) || $option['budget_mode'] = $this->budget_mode;
        empty($this->budget) || $option['budget'] = $this->budget;

        return $this->request($option);
    }
}
