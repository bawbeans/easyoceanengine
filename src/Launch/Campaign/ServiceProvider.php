<?php

namespace EasyOceanEngine\Launch\Campaign;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

/**
 * Class ServiceProvider.
 */
class ServiceProvider implements ServiceProviderInterface
{
    /**
     * {@inheritdoc}.
     */
    public function register(Container $app)
    {
        $app['campaign'] = function ($app) {
            return new Client($app);
        };
        $app['campaign_get'] = function ($app) {
            return new Get($app);
        };
        $app['campaign_create'] = function ($app) {
            return new Create($app);
        };
        $app['campaign_update'] = function ($app) {
            return new Update($app);
        };
        $app['campaign_update_status'] = function ($app) {
            return new UpdateStatus($app);
        };
    }
}
