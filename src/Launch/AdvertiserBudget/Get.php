<?php

namespace EasyOceanEngine\Launch\AdvertiserBudget;

use EasyOceanEngine\Kernel\BaseClient;
use EasyOceanEngine\Kernel\Exceptions\Exception;
use GuzzleHttp\Exception\GuzzleException;

/**
 * 获取账户日预算
 *
 * 此接口可以获取广告主账号设置的预算类型与预算，可以一次查询100个广告主账号预算
 */
class Get extends BaseClient
{
    protected $uri = 'advertiser/budget/get/';
    protected $method = 'GET';

    /**
     * 广告主id列表，长度限制：[1,100]
     * @var array
     */
    protected $advertiser_ids = [];

    /**
     * 广告主id列表，长度限制：[1,100]
     *
     * @param array $advertiser_ids
     * @return $this
     */
    public function advertiserIds(array $advertiser_ids)
    {
        $this->advertiser_ids = $advertiser_ids;
        return $this;
    }

    /**
     * 执行请求
     *
     * @return array
     * @throws Exception
     * @throws GuzzleException
     */
    public function send()
    {
        $option = [
            'advertiser_ids' => $this->advertiser_ids,
        ];
        return $this->request($option);
    }
}
