<?php

namespace EasyOceanEngine\Launch\AdvertiserBudget;

use EasyOceanEngine\Kernel\BaseClient;
use EasyOceanEngine\Kernel\Exceptions\Exception;
use GuzzleHttp\Exception\GuzzleException;

/**
 * @property Get $get 获取
 * @property Update $update 更新
 */
class Client extends BaseClient
{
    /**
     * 获取账户日预算
     *
     * 此接口可以获取广告主账号设置的预算类型与预算，可以一次查询100个广告主账号预算
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function get($param = [], $token = null): array
    {
        return $this->setUri('advertiser/budget/get/')->setToken($token)->request($param);
    }


    /**
     * 更新账户日预算
     *
     * 此接口可以更新广告主账号设置的预算类型与预算
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function update($param = [], $token = null): array
    {
        return $this->setUri('advertiser/update/budget/')
            ->setToken($token)
            ->setPost()
            ->request($param);
    }

    /**
     * @param string $property
     *
     * @return mixed
     *
     * @throws Exception
     */
    public function __get(string $property)
    {
        if (isset($this->app["advertiser_budget_{$property}"])) {
            return $this->app["advertiser_budget_{$property}"];
        }

        $class = __NAMESPACE__ . '\\' . ucfirst($property);
        if (class_exists($class)) {
            return new $class($this->app);
        }

        throw new Exception(sprintf('No advertiser budget service named "%s".', $property));
    }
}
