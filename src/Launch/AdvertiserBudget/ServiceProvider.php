<?php

namespace EasyOceanEngine\Launch\AdvertiserBudget;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

/**
 * Class ServiceProvider.
 */
class ServiceProvider implements ServiceProviderInterface
{
    /**
     * {@inheritdoc}.
     */
    public function register(Container $app)
    {
        $app['advertiser_budget'] = function ($app) {
            return new Client($app);
        };
        $app['advertiser_budget_get'] = function ($app) {
            return new Get($app);
        };
        $app['advertiser_budget_update'] = function ($app) {
            return new Update($app);
        };
    }
}
