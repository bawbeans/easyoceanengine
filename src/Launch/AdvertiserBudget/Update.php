<?php

namespace EasyOceanEngine\Launch\AdvertiserBudget;

use EasyOceanEngine\Kernel\BaseClient;
use EasyOceanEngine\Kernel\Exceptions\Exception;
use GuzzleHttp\Exception\GuzzleException;

/**
 * 更新账户日预算
 *
 * 此接口可以更新广告主账号设置的预算类型与预算
 */
class Update extends BaseClient
{
    protected $uri = 'advertiser/update/budget/';
    protected $method = 'POST';
    protected $content_type = 'application/json';

    /**
     * 预算值，取值范围：1000～9999999.99
     * 当budget_mode=BUDGET_MODE_DAY时，必填 单位：元； 最小值：0； 精度：小数点后两位 举例：100.01
     * @var string
     */
    protected $budget;

    /**
     * 预算模式
     *
     * BUDGET_MODE_INFINITE 不限
     * BUDGET_MODE_DAY 日预算
     * BUDGET_MODE_TOTAL 总预算
     * @var string
     */
    private $budget_mode;

    /**
     * @throws Exception
     */
    public function budgetMode(string $budget_mode)
    {
        if (!in_array($budget_mode, ['BUDGET_MODE_INFINITE', 'BUDGET_MODE_DAY', 'BUDGET_MODE_TOTAL'])) {
            throw new Exception('预算模式参数不正确', 100);
        }
        $this->budget_mode = $budget_mode;
        return $this;
    }

    public function budget(string $budget)
    {
        $this->budget = $budget;
        return $this;
    }

    /**
     * 执行请求
     *
     * @return array
     * @throws Exception
     * @throws GuzzleException
     */
    public function send()
    {
        $option = [
            'budget_mode' => $this->budget_mode,
        ];
        empty($this->budget) || $option['budget'] = $this->budget;
        if ($this->budget_mode == 'BUDGET_MODE_DAY' && empty($this->budget)) {
            throw new Exception('budget_mode = BUDGET_MODE_DAY 时，budget 必填', 100);
        }
        return $this->request($option);
    }
}
