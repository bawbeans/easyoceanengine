<?php

namespace EasyOceanEngine\Launch;

use EasyOceanEngine\Kernel\ServiceContainer;

/**
 * 广告投放
 *
 * @property AdvertiserBudget\Client $advertiser_budget 广告账户预算
 * @property Campaign\Client $campaign 广告组
 * @property Ad\Client $ad 广告计划
 * @property Creative\Client $creative 广告计划
 * @property AdvertiserBudget\Get $advertiser_budget_get 获取账户日预算
 * @property AdvertiserBudget\Update $advertiser_budget_update 更新账户日预算
 * @property Campaign\Create $campaign_create 创建广告组
 * @property Campaign\Get $campaign_get 获取广告组
 * @property Campaign\Update $campaign_update 更新广告组
 */
class Application extends ServiceContainer
{
    /**
     * @var array
     */
    protected $providers = [
        AdvertiserBudget\ServiceProvider::class,
        Campaign\ServiceProvider::class,
        Ad\ServiceProvider::class,
        Creative\ServiceProvider::class,
    ];
}
