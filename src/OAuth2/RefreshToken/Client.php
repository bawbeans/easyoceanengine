<?php

namespace EasyOceanEngine\OAuth2\RefreshToken;

use EasyOceanEngine\Kernel\Exceptions\Exception;
use EasyOceanEngine\Kernel\TokenBaseClient;
use GuzzleHttp\Exception\GuzzleException;

/**
 * 刷新Refresh Token
 *
 * 由于Access_Token有效期（默认1天）较短,当Access_Token超时后，可以使用refresh_token进行刷新，每次刷新都会产生新的access_token和Refresh_Token，同时重置二者的有效期。
 */
class Client extends TokenBaseClient
{
    protected $uri = 'oauth2/access_token/';
    protected $method = 'POST';

    /**
     * 授权码，在授权完成后回调时会提供该授权码，只有10分钟有效期，且只能使用一次，获取详情可见OAuth2.0授权
     * @var string
     */
    protected $auth_code;

    /**
     * 授权码，在授权完成后回调时会提供该授权码，只有10分钟有效期，且只能使用一次，获取详情可见OAuth2.0授权
     * @param string $auth_code
     * @return $this
     */
    public function authCode(string $auth_code)
    {
        $this->auth_code = $auth_code;
        return $this;
    }

    /**
     * @throws Exception|GuzzleException
     */
    public function send()
    {
        return $this->request([
            'app_id' => $this->app_id,
            'secret' => $this->secret,
            'grant_type' => $this->grant_type,
            'auth_code' => $this->auth_code,
        ]);
    }
}
