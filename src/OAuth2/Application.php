<?php

namespace EasyOceanEngine\OAuth2;

use EasyOceanEngine\Kernel\ServiceContainer;

/**
 * Token管理
 *
 * @property AccessToken\client $access_token
 * @property AppAccessToken\client $app_access_token
 * @property AdvertiserGet\client $advertiser_get
 * @property RefreshToken\client $refresh_token
 * @property TokenManage\client $token_manage
 */
class Application extends ServiceContainer
{
    /**
     * @var array
     */
    protected $providers = [
        AccessToken\ServiceProvider::class,
        AppAccessToken\ServiceProvider::class,
        AdvertiserGet\ServiceProvider::class,
        RefreshToken\ServiceProvider::class,
        TokenManage\ServiceProvider::class,
    ];
}
