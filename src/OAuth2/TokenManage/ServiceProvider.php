<?php

namespace EasyOceanEngine\OAuth2\TokenManage;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

/**
 * 数据源文件
 * Class ServiceProvider.
 */
class ServiceProvider implements ServiceProviderInterface
{
    /**
     * {@inheritdoc}.
     */
    public function register(Container $app)
    {
        $app['token_manage'] = function ($app) {
            return new Client($app);
        };
    }
}
