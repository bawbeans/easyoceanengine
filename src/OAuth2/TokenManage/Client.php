<?php

namespace EasyOceanEngine\OAuth2\TokenManage;

use EasyOceanEngine\Kernel\BaseClient;
use EasyOceanEngine\Kernel\Exceptions\Exception;
use GuzzleHttp\Exception\GuzzleException;

/**
 * 数据源文件
 */
class Client extends BaseClient
{
    /**
     * 获取Access Token
     * Access-Token是调用授权关系接口的调用凭证，用于服务端对API请求鉴权。所有接口均通过请求参数中传递的 Access_Token来进行身份认证和鉴权。
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function accessToken($param): array
    {
        return $this->setVersion('')->setUri('oauth2/access_token')->setPost()->request($param);
    }

    /**
     * 刷新Refresh Token
     * 由于Access_Token有效期（默认1天）较短,当Access_Token超时后，可以使用refresh_token进行刷新，每次刷新都会产生新的access_token和Refresh_Token，同时重置二者的有效期。
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function refreshToken($param): array
    {
        return $this->setVersion('')->setUri('oauth2/refresh_token')->setPost()->request($param);
    }

    /**
     * 获取已授权账户
     * 此接口用于获取已经授权的账号列表，账号包含了广告主、纵横组织、代理商等角色；
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function getAdvertiser($param): array
    {
        return $this->setVersion('')->setContentType()->setUri('oauth2/advertiser/get')->request($param);
    }

    /**
     * 获取授权User信息
     * API授权是以User为维度的，Access Token记录了授权User信息；通过此接口可以获取每一个Access Token对应的User信息，方便开发者区分以及管理对应授权关系；
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function userInfo($param, $token = null): array
    {
        return $this->setUri('user/info')->setToken($token)->request($param);
    }

    /**
     * 获取APP Access Token
     * 应用级token获取
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function appAccessToken($param): array
    {
        return $this->setVersion('')->setUri('oauth2/app_access_token')->request($param);
    }

}
