<?php

namespace EasyOceanEngine\OAuth2\AccessToken;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

/**
 * Class ServiceProvider.
 */
class ServiceProvider implements ServiceProviderInterface
{
    /**
     * {@inheritdoc}.
     */
    public function register(Container $app)
    {
        $app['access_token'] = function ($app) {
            return new Client($app);
        };
    }
}
