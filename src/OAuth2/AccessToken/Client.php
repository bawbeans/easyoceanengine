<?php

namespace EasyOceanEngine\OAuth2\AccessToken;

use EasyOceanEngine\Kernel\Exceptions\Exception;
use EasyOceanEngine\Kernel\TokenBaseClient;
use GuzzleHttp\Exception\GuzzleException;

/**
 * 获取Access Token
 *
 * Access-Token是调用授权关系接口的调用凭证，用于服务端对API请求鉴权。所有接口均通过请求参数中传递的 Access_Token来进行身份认证和鉴权。
 */
class Client extends TokenBaseClient
{
    protected $uri = 'oauth2/access_token/';
    protected $method = 'POST';

    /**
     * 授权码，在授权完成后回调时会提供该授权码，只有10分钟有效期，且只能使用一次，获取详情可见OAuth2.0授权
     * @var string
     */
    protected $auth_code;

    /**
     * 授权码，在授权完成后回调时会提供该授权码，只有10分钟有效期，且只能使用一次，获取详情可见OAuth2.0授权
     * @param string $auth_code
     * @return $this
     */
    public function authCode(string $auth_code)
    {
        $this->auth_code = $auth_code;
        return $this;
    }

    /**
     * @throws Exception|GuzzleException
     */
    public function send()
    {
        return $this->request([
            'app_id' => $this->app_id,
            'secret' => $this->secret,
            'grant_type' => $this->grant_type,
            'auth_code' => $this->auth_code,
        ]);
    }
}
