<?php

namespace EasyOceanEngine\OAuth2\AppAccessToken;

use EasyOceanEngine\Kernel\Exceptions\Exception;
use EasyOceanEngine\Kernel\TokenBaseClient;
use GuzzleHttp\Exception\GuzzleException;

/**
 * 获取APP Access Token
 *
 * 应用级token获取
 */
class Client extends TokenBaseClient
{
    protected $uri = 'oauth2/app_access_token/';
    protected $method = 'POST';

    /**
     * @throws Exception|GuzzleException
     */
    public function send()
    {
        return $this->request([
            'app_id' => $this->app_id,
            'secret' => $this->secret,
        ]);
    }
}
