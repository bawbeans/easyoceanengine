<?php

namespace EasyOceanEngine\OAuth2\AppAccessToken;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

/**
 * Class ServiceProvider.
 */
class ServiceProvider implements ServiceProviderInterface
{
    /**
     * {@inheritdoc}.
     */
    public function register(Container $app)
    {
        $app['app_access_token'] = function ($app) {
            return new Client($app);
        };
    }
}
