<?php

namespace EasyOceanEngine\OAuth2\AdvertiserGet;

use EasyOceanEngine\Kernel\Exceptions\Exception;
use EasyOceanEngine\Kernel\TokenBaseClient;
use GuzzleHttp\Exception\GuzzleException;

/**
 * 获取已授权账户
 *
 * 此接口用于获取已经授权的账号列表，账号包含了广告主、纵横组织、代理商等角色；
 */
class Client extends TokenBaseClient
{
    protected $uri = 'oauth2/advertiser/get/';
    protected $method = 'GET';


    /**
     * 授权access_token，从"获取Access Token"和“刷新Access Token”的返回结果中得到
     * @var string
     */
    protected $access_token = '';

    /**
     * 授权码，在授权完成后回调时会提供该授权码，只有10分钟有效期，且只能使用一次，获取详情可见OAuth2.0授权
     * @param string $access_token
     * @return $this
     */
    public function accessToken(string $access_token)
    {
        $this->access_token = $access_token;
        return $this;
    }

    /**
     * @throws Exception|GuzzleException
     */
    public function send()
    {
        return $this->request([
            'app_id' => $this->app_id,
            'secret' => $this->secret,
            'access_token' => $this->access_token,
        ]);
    }
}
