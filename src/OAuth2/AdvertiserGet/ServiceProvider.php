<?php

namespace EasyOceanEngine\OAuth2\AdvertiserGet;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

/**
 * Class ServiceProvider.
 */
class ServiceProvider implements ServiceProviderInterface
{
    /**
     * {@inheritdoc}.
     */
    public function register(Container $app)
    {
        $app['advertiser_get'] = function ($app) {
            return new Client($app);
        };
    }
}
