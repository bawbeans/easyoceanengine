<?php

namespace EasyOceanEngine\DMP;

use EasyOceanEngine\Kernel\ServiceContainer;

/**
 * DMP人群管理
 * @property CustomAudience\Client $custom_audience 人群包
 * @property DataSource\Client $data_source 资源包
 */
class Application extends ServiceContainer
{
    /**
     * @var array
     */
    protected $providers = [
        DataSource\ServiceProvider::class,
        CustomAudience\ServiceProvider::class,
    ];
}
