<?php

namespace EasyOceanEngine\DMP\DataSource;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

/**
 * 数据源文件
 * Class ServiceProvider.
 */
class ServiceProvider implements ServiceProviderInterface
{
    /**
     * {@inheritdoc}.
     */
    public function register(Container $app)
    {
        $app['data_source'] = function ($app) {
            return new Client($app);
        };
    }
}
