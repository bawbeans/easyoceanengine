<?php

namespace EasyOceanEngine\DMP\DataSource;

use EasyOceanEngine\Kernel\BaseClient;
use EasyOceanEngine\Kernel\Exceptions\Exception;
use GuzzleHttp\Exception\GuzzleException;

/**
 * 数据源文件
 */
class Client extends BaseClient
{
    /**
     * 数据源文件上传
     * 当用户需要上传本地数据到DMP数据平台上时，需要使用数据源文件上传功能。用户上传数据源文件后会返回文件路径file_path，用于调用【数据源创建】时创建相应的数据源。
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function upload($param = [], $token = null): array
    {
        return $this->setUri('dmp/data_source/file/upload/')
            ->setPost()
            ->setContentType('multipart/form-data')
            ->setToken($token)->request($param);
    }

    /**
     * 数据源创建
     * 通过【数据源文件上传】接口得到file_path文件路径后，调用当前接口将数据源文件创建成一个数据源，创建成功后会返回一个数据源id，作为数据源的唯一标识。
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function create($param = [], $token = null): array
    {
        return $this->setUri('dmp/data_source/create')
            ->setPost()
            ->setToken($token)->request($param);
    }

    /**
     * 数据源更新
     *
     * 用户可以调用该接口在原有的数据源上进行添加、删除、重置操作。
     * 数据源更新不会导致数据源id发生变化。用户可以在【数据源详细信息】查看更新是否完成。
     * 当lastest_published_time:数据源最近一次发布时间返回的数据被覆盖为最新的更新发布时间后，则说明最近一次更新已经完成。
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function update($param = [], $token = null): array
    {
        return $this->setUri('dmp/data_source/update')
            ->setPost()
            ->setContentType('multipart/form-data')
            ->setToken($token)->request($param);
    }

    /**
     * 数据源详细信息
     *
     * 通过数据源id，查询该数据源相关信息和其对应的人群包信息。数据源解析完成后会返回default_audience：数据源对应的人群包相关信息，其中人群包ID是该人群包的唯一标识。解析未完成不返回default_audience：数据源对应的人群包相关信息。
     * @throws GuzzleException
     * @throws Exception
     */
    public function read($param = [], $token = null): array
    {
        return $this->setUri('dmp/data_source/read')->setToken($token)->request($param);
    }
}
