<?php

namespace EasyOceanEngine\DMP\CustomAudience;

use EasyOceanEngine\Kernel\BaseClient;
use EasyOceanEngine\Kernel\Exceptions\Exception;
use GuzzleHttp\Exception\GuzzleException;

/**
 * 人群包
 */
class Client extends BaseClient
{
    /**
     * 人群包列表
     * 通过此接口你可以查询广告主下存在的人群包列表和信息。
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function select($param = [], $token = null): array
    {
        return $this->setUri('dmp/custom_audience/select')->setToken($token)->request($param);
    }

    /**
     * 人群包详细信息
     * 用户可以通过调用此接口，查询广告主下的指定人群包信息。支持查询已删除的人群包信息，具体包含的信息内容请查看应答参数。
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function read($param = [], $token = null): array
    {
        return $this->setUri('dmp/custom_audience/read')->setToken($token)->request($param);
    }

    /**
     * 发布人群包
     * 使用发布接口可以将人群包进行发布。
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function publish($param = [], $token = null): array
    {
        return $this->setUri('dmp/custom_audience/publish')
            ->setPost()
            ->setToken($token)->request($param);
    }

    /**
     * 推送人群包
     * 每个人群包生成一个人群包id后，都需要经过推送，才可以在被推送的广告主下使用。同时，推送人群包可以将人群包共享给同主体的广告主。
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function push($param = [], $token = null): array
    {
        return $this->setUri('dmp/custom_audience/push_v2')
            ->setPost()
            ->setToken($token)->request($param);
    }

    /**
     * 删除人群包
     * 通过此接口可做人群包删除操作。已经在计划中使用的人群包不能被删除，只有该计划被删除后，人群包才可以删除。
     *
     * @throws GuzzleException
     * @throws Exception
     */
    public function delete($param = [], $token = null): array
    {
        return $this->setUri('dmp/custom_audience/delete')
            ->setPost()
            ->setToken($token)->request($param);
    }
}
