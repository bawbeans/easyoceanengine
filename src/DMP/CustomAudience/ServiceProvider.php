<?php

namespace EasyOceanEngine\DMP\CustomAudience;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

/**
 * 人群包
 * Class ServiceProvider.
 */
class ServiceProvider implements ServiceProviderInterface
{
    /**
     * {@inheritdoc}.
     */
    public function register(Container $app)
    {
        $app['custom_audience'] = function ($app) {
            return new Client($app);
        };
    }
}
