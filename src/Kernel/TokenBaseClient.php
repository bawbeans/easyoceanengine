<?php

namespace EasyOceanEngine\Kernel;

use EasyOceanEngine\Kernel\Exceptions\Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

class TokenBaseClient
{
    protected const ENDPOINT = 'https://open.oceanengine.com/open_api/';
    protected $uri;
    protected $method = 'GET';
    protected $client;
    protected $app;

    /**
     * 开发者申请的应用APP_ID，可通过“应用管理”界面查看
     * @var string
     */
    protected $app_id;

    /**
     * 开发者应用的私钥Secret，可通过“应用管理”界面查看（确保填入secret与app_id对应以免报错！）
     * @var string
     */
    protected $secret = '';

    /**
     * 请求headers 的 Content-Type 值
     * @var string
     */
    protected $content_type = 'application/json';

    /**
     * 授权类型。允许值: "auth_code"
     * @var string
     */
    protected $grant_type = 'auth_code';

    public function __construct(ServiceContainer $app)
    {
        $this->app = $app;
        $config = $app->getConfig();
        $this->app_id = $config['app_id'];
        $this->secret = $config['secret'];
        $this->client = $this->client();
    }

    public function client(): Client
    {
        return new Client(['base_uri' => static::ENDPOINT, 'verify' => false]);
    }

    /**
     * 授权类型。允许值: "auth_code"
     * @param string $grant_type
     * @return \EasyOceanEngine\OAuth2\AccessToken\Client
     */
    public function grantType(string $grant_type)
    {
        $this->grant_type = $grant_type;
        return $this;
    }

    /**
     * @throws GuzzleException
     * @throws Exception
     */
    public function request($option = []): array
    {
        $client = $this->client();
        $headers = [];
        $headers['Content-Type'] = $this->content_type;
        $response = $client->request($this->method, $this->uri, [
            'json' => $option,
            "headers" => $headers,
        ]);
        if ($response->getStatusCode() != 200) {
            throw new Exception('请求错误', $response->getStatusCode());
        }

        $json = $response->getBody()->getContents();
        $data = json_decode($json, true);
        if ($data['code'] != 0) {
            throw new Exception($data['message'], $data['code']);
        }
        return $data['data'];
    }
}
