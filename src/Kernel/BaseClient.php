<?php

namespace EasyOceanEngine\Kernel;

use EasyOceanEngine\Kernel\Exceptions\Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

class BaseClient
{
    protected const ENDPOINT = 'https://ad.oceanengine.com/open_api';
    protected $version = 2;
    protected $token;
    protected $uri;
    protected $method = 'GET';
    protected $client;
    protected $app;
    /**
     * 请求headers 的 Content-Type 值
     * @var string
     */
    protected $content_type;

    /**
     * 广告主ID
     * @var int
     */
    protected $advertiser_id;

    public function __construct(ServiceContainer $app)
    {
        $this->app = $app;
        $this->init();
    }

    protected function init()
    {
        $config = $this->app->getConfig();
        $this->token = $token ?? $config['token'] ?? '';
        empty($config['advertiser_id']) || $this->advertiserId($config['advertiser_id']);
        $this->uri = null;
        $this->method = 'GET';
        $this->content_type = null;
        $this->version = 2;
    }

    /**
     * 设置广告组
     * @param int $advertiser_id
     * @return $this
     */
    public function advertiserId(int $advertiser_id)
    {
        $this->advertiser_id = $advertiser_id;
        return $this;
    }

    /**
     * 设置版本
     * @param string $version
     * @return $this
     */
    protected function setVersion(string $version)
    {
        $this->version = $version;
        return $this;
    }

    protected function client(): Client
    {
        $ar = [];
        $ar[] = static::ENDPOINT;
        empty($this->version) || $ar[] = $this->version;
        $base_uri = implode('/', $ar) . '/';
        return new Client(['base_uri' => $base_uri, 'verify' => false]);
    }

    /**
     * 请求地址
     * @param string $uri
     * @return $this
     */
    protected function setUri(string $uri)
    {
        $this->uri = trim($uri, '/') . '/';
        return $this;
    }

    /**
     * 请求方法
     * @return $this
     */
    protected function setPost()
    {
        $this->method = 'POST';
        $this->setContentType();
        return $this;
    }

    /**
     * 请求token
     * @return $this
     */
    public function setToken($token)
    {
        if (empty($token)) {
            return $this;
        }
        $this->token = $token;
        return $this;
    }

    /**
     * header Content-Type 信息
     * @return $this
     */
    protected function setContentType($content_type = 'application/json')
    {
        $this->content_type = $content_type;
        return $this;
    }

    /**
     * @throws GuzzleException
     * @throws Exception
     */
    protected function request($param = []): array
    {
        $client = $this->client();
        $headers = [];
        empty($this->token) || $headers['Access-Token'] = $this->token;
        empty($this->content_type) || $headers['Content-Type'] = $this->content_type;
        if (empty($param['advertiser_id']) && empty($param['agent_id'])) {
            $param['advertiser_id'] = $this->advertiser_id;
        }
        $options = [];
        $options['headers'] = $headers;
        if ($this->method == 'GET') {
            foreach ($param as &$v) {
                is_array($v) && $v = json_encode($v);
            }
            unset($v);
            $options['query'] = $param;
        }
        if ($this->method == 'POST') {
            $options['json'] = $param;
        }
        $response = $client->request($this->method, $this->uri, $options);
        $this->init();
        if ($response->getStatusCode() != 200) {
            throw new Exception('请求错误', $response->getStatusCode());
        }

        $json = $response->getBody()->getContents();
        $data = json_decode($json, true);
        if ($data['code'] != 0) {
            throw new Exception($data['message'], $data['code']);
        }
        if (!isset($data['data'])) {
            return $data;
        }
        return $data['data'];
    }
}
