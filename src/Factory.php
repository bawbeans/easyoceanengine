<?php

namespace EasyOceanEngine;

use EasyOceanEngine\Kernel\ServiceContainer;
use function EasyOceanEngine\Kernel\underline2camel;

/**
 * @method static AccountServices\Application accountServices(array $config = []) 账号服务
 * @method static Material\Application material(array $config = []) 素材管理
 * @method static OAuth2\Application OAuth2(array $config = []) Oauth2.0授权
 * @method static Launch\Application launch(array $config = []) 广告投放
 * @method static DataReport\Application dataReport(array $config = []) 数据报告
 * @method static DMP\Application d_m_p(array $config = []) DMP人群管理
 * @method static Tools\Application tools(array $config = []) 工具类
 * @method static SearchLaunch\Application searchLaunch(array $config = []) 搜索广告投放
 * @method static Assets\Application assets(array $config = []) 资产
 * @method static Site\Application site(array $config = []) 建站管理
 * @method static V30\Application V30(array $config = []) 巨量广告升级版
 */
class Factory
{
    /**
     * @param string $name
     * @param array $config
     * @return ServiceContainer;
     */
    public static function make(string $name, array $config = [])
    {
        $namespace = underline2camel($name);
        $application = "EasyOceanEngine\\{$namespace}\\Application";

        return new $application($config);
    }

    /**
     * Dynamically pass methods to the application.
     *
     * @param string $name
     * @param array $arguments
     *
     * @return mixed
     */
    public static function __callStatic($name, $arguments)
    {
        return self::make($name, ...$arguments);
    }
}
