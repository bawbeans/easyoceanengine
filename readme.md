
根据巨量文档菜单进行文件分类。

```PHP
$factory = Factory::dataReport(); // 巨量文档以及菜单分类
$advertisingData = $factory->advertising_data; // 巨量文档二级分类
$data = $advertisingData->advertiser([ // 巨量文档具体哪个接口
    'advertiser_id' => 'xxxx',
    'start_date' => '2022-01-13',
    'end_date' => '2022-01-13',
    'page' => 1,
    'page_size' => 1000,
],$token);
```
